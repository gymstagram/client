import React from "react";
import { Route, Switch } from "react-router-dom";
import { ExploreContainer } from "../components/explore";
import { LoginContainer } from "../components/login";
import { TrainingsContainer, TrainingSortContainer } from "../components/trainings";
import { MyTrainingsContainer, TrainingExecerisesContainer } from "../components/trainings/myTrainings";
import { PersonalCustomizationContainer } from "../components/trainings/personalCustomization";
import { PrivateRoute } from "./privateRoute";

export const allRoutes = (
  <Switch>
    <PrivateRoute path="/" exact component={ExploreContainer} />
    <Route path="/login" exact component={LoginContainer} />
    <PrivateRoute path="/explore" exact component={ExploreContainer} />
    <PrivateRoute path="/trainings" exact component={TrainingsContainer} />
    {/* <PrivateRoute path="/trainings/:type/:muscle" exact={false} component={TrainingsContainer} /> */}
    <PrivateRoute path="/trainings/new" exact component={() => <TrainingSortContainer sortBy="createdAt" />} />
    <PrivateRoute path="/trainings/mostPopular" exact component={() => <TrainingSortContainer sortBy="numOfLikes" />} />
    <PrivateRoute path="/trainings/personalCustomize" exact component={PersonalCustomizationContainer} />
    <PrivateRoute path="/trainings/personalCustomize/:muscle" exact component={PersonalCustomizationContainer} />
    <PrivateRoute path="/trainings/myTraining" exact component={MyTrainingsContainer} />
    <PrivateRoute path="/trainings/myTraining/:day" exact component={TrainingExecerisesContainer} />
  </Switch>
);

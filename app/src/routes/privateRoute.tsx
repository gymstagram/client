import React from "react";
import { Redirect, Route, useLocation, useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { usersSelector } from "../slices/users/usersSlice";
import { TabsContainer } from "../components/tabs";
import jwt from "jsonwebtoken";
import { Button } from "@material-ui/core";
import { useToasts } from "react-toast-notifications";
import { TrainingsTabsContainer } from "../components/trainings";
import { PersonalCustomizationContainer } from "../components/trainings/personalCustomization";
import { MyTrainingsContainer } from "../components/trainings/myTrainings";

interface Props {
  component: React.FC;
  path: string;
  exact: boolean;
}

export const PrivateRoute: React.FC<Props> = ({ component: Component, ...rest }) => {
  const location = useLocation();
  const history = useHistory();
  const { accessToken } = useSelector(usersSelector);
  const { username } = useSelector(usersSelector);
  const { addToast } = useToasts();

  const isTokenValid = () => {
    let isValid = true;
    jwt.verify(accessToken, process.env.REACT_APP_ACCESS_TOKEN_SECRET!, (err: any, decoded: any) => {
      if (err) {
        isValid = false;
        addToast(err.toString(), {
          appearance: "error",
          autoDismiss: true,
        });
      }
    });
    return isValid;
  };

  const handleLogOut = () => {
    localStorage.clear();
    history.push("/login");
  };

  return (
    <Route {...rest}>
      {isTokenValid() ? (
        <React.Fragment>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div
              style={{
                color: "white",
                fontSize: "1.7rem",
                margin: "1rem 1rem 1rem 1rem",
              }}
            >{`Welcome ${username} !`}</div>
            <div>
              <Button
                color="secondary"
                onClick={handleLogOut}
                variant="contained"
                style={{
                  marginBottom: "1rem",
                  marginTop: "1rem",
                  marginRight: "1rem",
                }}
              >
                Log Out
              </Button>
            </div>
          </div>
          <TabsContainer />
          {rest.path.includes("/trainings") && <TrainingsTabsContainer />}

          {/* {rest.path.includes("/trainings/personalCustomize") && <PersonalCustomizationContainer />} */}
          {rest.path.includes("/trainings/myTraining") && <MyTrainingsContainer />}
          <Component />
        </React.Fragment>
      ) : (
        <Redirect to={{ pathname: "/login", state: { from: location } }} />
      )}
    </Route>
  );
};

import React from "react";
import ReactDOM from "react-dom";
// import "./index.css";
import { ToastProvider } from "react-toast-notifications";
import "./styles.scss";
import { BrowserRouter } from "react-router-dom";
import { allRoutes } from "./routes";
import { Provider } from "react-redux";
import store from "./slices/configureStore";
import "primereact/resources/themes/saga-blue/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import { AlertOnNewItemAdded } from "./utils/AlertOnNewItemAdded";

ReactDOM.render(
  <Provider store={store}>
    <ToastProvider>
      <BrowserRouter>
        <>
          <AlertOnNewItemAdded />
          {allRoutes}
        </>
      </BrowserRouter>
    </ToastProvider>
  </Provider>,
  document.getElementById("root")
);

import { error } from "console";
import { loadState } from "../slices/localStroge";

export const GetFetchAPI = async (route: string) => {
  let response;
  try {
    response = await fetch(`${process.env.REACT_APP_SERVER_HOST}${route}`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + loadState("accessToken"),
        "Content-Type": "application/json",
      },
    });
  } catch (error) {
    throw Error("Server is down, try again later!");
  }

  if (response.status !== 200) {
    throw Error((await response.json()).error);
  } else {
    const data = await response.json();
    if (data.entities) {
      return data.entities;
    } else {
      return data;
    }
  }
};

export const PostFetchAPI = async (dataToSend: any, route: string) => {
  let response;
  try {
    response = await fetch(`${process.env.REACT_APP_SERVER_HOST}${route}`, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + loadState("accessToken"),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(dataToSend),
    });
  } catch (error) {
    throw Error("Server is down, try again later!");
  }

  if (response.status !== 200) {
    throw Error((await response.json()).error);
  } else {
    const data = await response.json();
    if (data.entities) {
      return data.entities;
    } else {
      return data;
    }
  }
};

export const DeleteFetchAPI = async (route: string) => {
  let response;
  try {
    response = await fetch(`${process.env.REACT_APP_SERVER_HOST}${route}`, {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + loadState("accessToken"),
        "Content-Type": "application/json",
      },
    });
  } catch (error) {
    throw Error("Server is down, try again later!");
  }

  if (response.status !== 200) {
    throw Error((await response.json()).error);
  } else {
    const data = await response.json();
    return data;
  }
};
export const PutFetchAPI = async (dataToSend: any, route: string) => {
  let response;
  try {
    response = await fetch(`${process.env.REACT_APP_SERVER_HOST}${route}`, {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + loadState("accessToken"),
        "Content-Type": "application/json",
      },
      body: JSON.stringify(dataToSend),
    });
  } catch (error) {
    throw Error("Server is down, try again later!");
  }

  if (response.status !== 200) {
    throw Error((await response.json()).error);
  } else {
    const data = await response.json();
    return data;
  }
};

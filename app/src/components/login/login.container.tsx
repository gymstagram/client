import React, { useEffect, useState } from "react";
import "./login.container.scss";
import { InputText } from "primereact/inputtext";
import { User } from "./login.interface";
import { Button } from "primereact/button";
import { logingUser, signUpUser } from "../../slices/users/actions";
import { useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { PostFetchAPI } from "../../services/fetchAPI";
import { useToasts } from "react-toast-notifications";

export const LoginContainer: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [loginUsername, setLoginUsername] = useState<string>("");
  const [loginPassword, setLoginPassword] = useState<string>("");
  const [signUpUsername, setSignUpUsername] = useState<string>("");
  const [signUpPassword, setSignUpPassword] = useState<string>("");
  const [openTab, setOpenTab] = useState(0);
  const { addToast } = useToasts();

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setOpenTab(newValue);
  };

  const handleLogin = async () => {
    try {
      await dispatch(logingUser(loginUsername, loginPassword));
      history.push("/explore");
    } catch (error) {
      addToast(error.message.toString(), {
        appearance: "error",
        autoDismiss: true,
      });
    }
  };

  const handleSignUp = async () => {
    await PostFetchAPI(
      { username: signUpUsername, password: signUpPassword },
      `/api/users/signup`
    )
      .then((data) => {
        if (data) {
          setSignUpUsername("");
          setSignUpPassword("");
          addToast("create new user", {
            appearance: "success",
            autoDismiss: true,
          });
        }
      })
      .catch((error) => {
        addToast(error.toString(), {
          appearance: "error",
          autoDismiss: true,
        });
      });
  };

  return (
    <div
      style={{
        width: "50%",
        marginLeft: "25%",
        marginTop: "11rem",
        backgroundColor: "#3e4053",
        height: "25rem",
      }}
    >
      <Paper className="root">
        <Tabs
          value={openTab}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          classes={{ flexContainer: "flexContainer" }}
        >
          <Tab classes={{ wrapper: "tabColor" }} label="Login" />
          <Tab classes={{ wrapper: "tabColor" }} label="Sign Up" />
        </Tabs>
      </Paper>

      <div className="loginSignUP">
        {openTab === 0 ? (
          <div className="form">
            <span className="form__login">login</span>
            <span className="p-float-label form__username">
              <InputText
                id="username"
                value={loginUsername}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  setLoginUsername(e.target.value)
                }
              />
              <label htmlFor="username">Username</label>
            </span>
            <span className="p-float-label form__password">
              <InputText
                type="password"
                id="password"
                value={loginPassword}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  setLoginPassword(e.target.value)
                }
              />
              <label htmlFor="password">Password</label>
            </span>
            <Button label="Login" onClick={handleLogin} />
          </div>
        ) : (
          <div className="form">
            <span className="form__login">SignUp</span>
            <span className="p-float-label form__username">
              <InputText
                id="username"
                value={signUpUsername}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  setSignUpUsername(e.target.value)
                }
              />
              <label htmlFor="username">Username</label>
            </span>
            <span className="p-float-label form__password">
              <InputText
                type="password"
                id="password"
                value={signUpPassword}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  setSignUpPassword(e.target.value)
                }
              />
              <label htmlFor="username">Password</label>
            </span>
            <Button label="Sign Up" onClick={handleSignUp} />
          </div>
        )}
      </div>
    </div>
  );
};

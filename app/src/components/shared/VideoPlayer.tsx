import { CircularProgress } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import ReactPlayer from "react-player";

interface VideoPlayerProps {
  videoURL: string;
}

export const VideoPlayer: React.FC<VideoPlayerProps> = ({ videoURL }) => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  //   useEffect(() => {
  //     videoURL && alert(videoURL);
  //   }, [videoURL]);

  return (
    <>
      <ReactPlayer
        style={{ display: isLoading ? "none" : "block" }}
        onReady={() => setIsLoading(false)}
        width="100%"
        height="100%"
        url={videoURL}
      />
      <div>{isLoading && <CircularProgress size={70} />}</div>
    </>
  );
};

import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { useHistory, useLocation } from "react-router-dom";
import "./tabs.scss";

export const TabsContainer: React.FC = () => {
  const history = useHistory();
  const [value, setValue] = useState<number>(0);
  const location: any = useLocation();

  useEffect(() => {
    if (location.pathname.includes("/trainings")) {
      setValue(1);
      history.push("/trainings");
    } else {
      setValue(0);
      history.push("/explore");
    }
  }, []);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
    if (newValue === 0) {
      history.push("/explore");
    } else {
      history.push("/trainings");
    }
  };

  return (
    <Paper className="root">
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        classes={{ flexContainer: "flexContainer" }}
      >
        <Tab classes={{ wrapper: "tabColor" }} label="Explore" />
        <Tab classes={{ wrapper: "tabColor" }} label="Trainings" />
      </Tabs>
    </Paper>
  );
};

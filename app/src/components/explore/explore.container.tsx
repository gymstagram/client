import { Alert } from "@material-ui/lab";
import { IPost } from "gymstagram-common";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import { POSTS_ENDPOINT } from "../../consts/endpoints";
import { GetFetchAPI } from "../../services/fetchAPI";
import "../explore/posts/explore.scss";
import { PostContainer } from "./posts/post.container";

export const ExploreContainer: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [showAlert, setShowAlert] = useState(false);
  const { addToast } = useToasts();
  const [posts, setPosts] = useState<IPost[]>([]);
  useEffect(() => {
    async function getPosts() {
      GetFetchAPI(POSTS_ENDPOINT)
        .then((data) => {
          setPosts(
            data.sort(function compare(a: any, b: any) {
              let dateA: any = new Date(a.createdAt);
              let dateB: any = new Date(b.createdAt);
              return dateB - dateA;
            })
          );
        })
        .catch((error) => {
          addToast(error.toString(), {
            appearance: "error",
            autoDismiss: true,
          });
          history.push("/login");
        });
    }

    getPosts();
  }, []);

  const deletePost = (id: string) => {
    let filteredPosts = posts.filter((post) => {
      return post._id != id;
    });
    setPosts(filteredPosts);
    setShowAlert(true);
    console.log("setting to true");
    setTimeout(() => {
      console.log("setting to false");
      setShowAlert(false);
    }, 3000);
  };

  return (
    <>
      {showAlert ? (
        <Alert variant="filled" severity="success" className={"alert"}>
          Your post deleted successfully!
        </Alert>
      ) : null}
      <div className={"postContainer"}>
        {posts.map((post: IPost, index: number) => {
          return (
            <div key={index}>
              <PostContainer post={post} deletePost={deletePost} />
            </div>
          );
        })}
      </div>
    </>
  );
};

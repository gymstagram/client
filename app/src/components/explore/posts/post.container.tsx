import { Typography } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { IPost, ITraining } from "gymstagram-common";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import { POSTS_ENDPOINT } from "../../../consts/endpoints";
import UserImage from "../../../images/user.png";
import { DeleteFetchAPI } from "../../../services/fetchAPI";
import { Likes } from "../../../utils/Likes.container";
import { VideoPlayer } from "../../shared/VideoPlayer";
import { ShareTrainingModal } from "../../trainings/ShareTrainingModal";
import "./explore.scss";

type IPostProps = { post: IPost; deletePost: (id: string) => void };
export const PostContainer: React.FC<IPostProps> = (props) => {
  const history = useHistory();
  const { publisher, content, trainingID, _id } = { ...props.post };
  const { addToast } = useToasts();
  const [isLoading, setIsLoading] = useState(true);
  const [publisherDescription, setPublisherDescription] = useState(content);
  const handleDeletePost = (postId: string) => {
    DeleteFetchAPI(POSTS_ENDPOINT + "/" + postId)
      .then((data) => props.deletePost(postId))
      .catch((error) => {
        addToast(error.toString(), {
          appearance: "error",
          autoDismiss: true,
        });
        history.push("/login");
      });
  };
  return (
    <div className="container">
      <article className="Post">
        <div className="Post-image">
          <header>
            <div className="Post-user">
              <div className="Post-user-avatar">
                <img src={UserImage} alt={publisher} />
              </div>
              <div className="Post-user-nickname">
                <span>{publisher}</span>
              </div>
            </div>
          </header>
          <div className="Post-image-bg">
            <div className="post-header-text-and-edit">
              <Typography component={"span"} className="post-name">
                {((trainingID as unknown) as ITraining).name}
              </Typography>
              {localStorage.getItem("username") == publisher ? (
                <div className={"post-editing"}>
                  <div className={"icon"}>
                    <DeleteIcon color="secondary" fontSize="large" onClick={() => handleDeletePost(_id!)} />
                  </div>
                  <ShareTrainingModal
                    training={(trainingID as unknown) as ITraining}
                    type={"edit"}
                    content={publisherDescription}
                    postId={_id}
                    setPublisherDescription={setPublisherDescription}
                  />
                </div>
              ) : null}
            </div>

            <div className="post-video-player">
              <VideoPlayer videoURL={((trainingID as unknown) as ITraining).video} />
            </div>
          </div>
        </div>

        <div className="Post-caption">
          <div className="Post-user-description">
            <strong>{publisher}</strong> <span className={"post-description"}>{publisherDescription}</span>
          </div>

          <div className="post-likes">
            <Likes object={props.post} type={"post"} />
          </div>
        </div>
      </article>
    </div>
  );
};

export interface IPost {
  publisher: string;
  content: string;
  url: string;
  numOfLikes: string;
  createdAt: string;
}

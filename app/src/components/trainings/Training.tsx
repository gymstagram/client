import { Button, Typography } from "@material-ui/core";
import { ITraining, Difficulty } from "gymstagram-common";
import React from "react";
import { Likes } from "../../utils/Likes.container";
import "./training.scss";
import { ShareTrainingModal } from "./ShareTrainingModal";
import { VideoPlayer } from "../shared/VideoPlayer";

type ITrainingProps = { training: ITraining };
export const Training: React.FC<ITrainingProps> = (props) => {
  const { name, video, difficultyLevel } = { ...props.training };

  return (
    <div className="customPost">
      <Typography component={"span"} className="customPost__header" variant="h4">
        {name}
      </Typography>
      <div className="customPost__video">
        <VideoPlayer videoURL={video} />
      </div>

      <div className="customPost__likesAndDifficult">
        <Typography component={"span"} className="customPost__difficulty" variant="h6">
          Difficulty: {Difficulty[difficultyLevel]}
        </Typography>
        <Likes object={props.training} type={"training"} />
      </div>
      {/* <Button variant="contained" color="primary">
        Details of the training
      </Button> */}
      <ShareTrainingModal training={props.training} type={"share"} />
    </div>
  );
};

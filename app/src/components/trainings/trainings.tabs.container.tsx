import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Link, useHistory, useLocation, useParams } from "react-router-dom";

export const TrainingsTabsContainer: React.FC = () => {
  const [openTab, setOpenTab] = useState(0);
  const history = useHistory();
  const location: any = useLocation();

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setOpenTab(newValue);
  };

  useEffect(() => {
    if (location.pathname.includes("/trainings/new")) {
      setOpenTab(0);
      history.push(location.pathname);
    } else {
      if (location.pathname.includes("/trainings/mostPopular")) {
        setOpenTab(1);
        history.push(location.pathname);
      } else {
        if (location.pathname.includes("/trainings/personalCustomize")) {
          setOpenTab(2);
          history.push(location.pathname);
        } else {
          if (location.pathname.includes("/trainings/myTraining")) {
            setOpenTab(3);
            history.push(location.pathname);
          } else {
            setOpenTab(0);
            history.push("/trainings/new");
          }
        }
      }
    }
  }, []);

  return (
    <React.Fragment>
      <Paper className="root">
        <Tabs
          value={openTab}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          classes={{ flexContainer: "flexContainer" }}
        >
          <Tab classes={{ wrapper: "tabColor" }} label="New" component={Link} to="/trainings/new" />
          <Tab classes={{ wrapper: "tabColor" }} label="Most Pouplar" component={Link} to="/trainings/mostPopular" />
          <Tab
            classes={{ wrapper: "tabColor" }}
            label="Personal Customize"
            component={Link}
            to="/trainings/personalCustomize"
          />
          <Tab classes={{ wrapper: "tabColor" }} label="My Trainings" component={Link} to="/trainings/myTraining" />
        </Tabs>
      </Paper>
    </React.Fragment>
  );
};

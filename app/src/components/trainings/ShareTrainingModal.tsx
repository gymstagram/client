import {
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  TextField,
  Typography,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { ITraining } from "gymstagram-common";
import React, { Component, useEffect, useState } from "react";
import ReactPlayer from "react-player";
import { POSTS_ENDPOINT } from "../../consts/endpoints";
import { PostFetchAPI, PutFetchAPI } from "../../services/fetchAPI";
import EditIcon from "@material-ui/icons/Edit";
import "../explore/posts/explore.scss";
import "./share.scss";
import { useToasts } from "react-toast-notifications";
import { useHistory } from "react-router-dom";
import { VideoPlayer } from "../shared/VideoPlayer";
interface ShareTrainingModalProps {
  training: ITraining;
  type: string;
  content?: string;
  postId?: string;
  setPublisherDescription?: React.Dispatch<React.SetStateAction<string>>;
}

export const ShareTrainingModal: React.FC<ShareTrainingModalProps> = (props) => {
  const { addToast } = useToasts();
  const history = useHistory();
  let endPoint = `${POSTS_ENDPOINT}`;
  const logedInUser = localStorage.getItem("username");
  const [open, setOpen] = useState(false);
  const [description, setDescription] = useState("");
  const [showSpinner, setShowSpinner] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const { name, video, _id } = { ...props.training };
  const type = props.type;
  const postId = props.postId;
  const setPublisherDescription = props.setPublisherDescription!;

  useEffect(() => {
    const content = props.content;

    if (content) {
      setDescription(content);
    }
  }, []);

  const [isValiedDescription, setIsValidDescription] = useState(true);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setDescription("");
  };
  const handlePublish = async () => {
    setShowSpinner(true);
    let endpointPublish = type == "share" ? POSTS_ENDPOINT : POSTS_ENDPOINT + "/" + postId;
    if (description != "") {
      let result =
        type == "share"
          ? await PostFetchAPI(
              {
                publisher: logedInUser,
                trainingID: _id,
                content: description,
                likedBy: [],
                numOfLikes: 0,
                createdAt: new Date(),
              },
              endpointPublish
            ).catch((error) => {
              addToast(error.toString(), {
                appearance: "error",
                autoDismiss: true,
              });
              history.push("/login");
            })
          : await PutFetchAPI(
              {
                publisher: logedInUser,
                trainingID: _id,
                content: description,
                likedBy: [],
                numOfLikes: 0,
                createdAt: new Date(),
              },
              endpointPublish
            ).catch((error) => {
              addToast(error.toString(), {
                appearance: "error",
                autoDismiss: true,
              });
              history.push("/login");
            });
      if (type == "edit") {
        setPublisherDescription(description);
      }

      setOpen(false);
      setShowSpinner(false);
      setShowAlert(true);

      console.log("setting to true");
      window.setTimeout(() => {
        console.log("setting to false");
        setShowAlert(false);
      }, 3000);
    } else {
      setIsValidDescription(false);
      setShowSpinner(false);
    }
  };
  const handleDescriptionChange = (event: { target: { value: React.SetStateAction<string> } }) => {
    console.log(event.target.value);
    if (event.target.value != "") {
      setIsValidDescription(true);
    }
    setDescription(event.target.value);
  };

  return (
    <>
      {type == "share" ? (
        <Button onClick={handleClickOpen} variant="contained" color="secondary">
          Share
        </Button>
      ) : (
        <div className={"icon"}>
          <EditIcon color="secondary" fontSize="large" onClick={handleClickOpen} />
        </div>
      )}

      <Dialog classes={{ paperWidthSm: "paperWidthSm" }} open={open} onClose={handleClose}>
        <DialogContent>
          <div className={"share-modal"}>
            <div className="Post-user">
              <div className="Post-user-avatar">
                <img src="https://picsum.photos/id/237/200/300" alt={name} />
              </div>
              <div className="Post-user-nickname">
                <span>{logedInUser}</span>
              </div>
            </div>

            <TextField
              id="outlined-multiline-static"
              label="Description"
              multiline
              rows={4}
              variant="outlined"
              defaultValue={description}
              onChange={handleDescriptionChange}
              error={!isValiedDescription}
              helperText={!isValiedDescription && "Add description for your post"}
              color="secondary"
              fullWidth
              className="share-modal__description"
              inputProps={{ style: { color: "white" } }}
            />
            <Typography component={"span"} className="share-modal__training-name">
              {name}
            </Typography>
            <VideoPlayer videoURL={video} />
            {/* <ReactPlayer url={video} /> */}
          </div>
        </DialogContent>

        <DialogActions>
          {!showSpinner ? (
            <Button onClick={handlePublish} color="secondary">
              {type == "share" ? "Publish" : "Edit"}
            </Button>
          ) : (
            <CircularProgress />
          )}
        </DialogActions>
      </Dialog>

      {showAlert ? (
        <Alert variant="filled" severity="success">
          Your post {`${type == "share" ? "published" : "edited"}`} successfully!
        </Alert>
      ) : null}
    </>
  );
};

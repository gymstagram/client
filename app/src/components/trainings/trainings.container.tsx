import React from "react";
import { TrainingsTabsContainer } from "./trainings.tabs.container";

export const TrainingsContainer: React.FC = () => {
  return <TrainingsTabsContainer />;
};

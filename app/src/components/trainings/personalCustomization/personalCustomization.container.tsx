import React, { useEffect, useState } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import "./personalCustomization.scss";
import { IMuscleGroup } from "gymstagram-common";
import { GetFetchAPI } from "../../../services/fetchAPI";
import { Link, useHistory, useParams } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import { MusclesExecerisesContainer } from "./musclesExerices";
import { Box, Typography } from "@material-ui/core";

function a11yProps(index: any) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      style={{
        overflowY: "auto",
        width: "100vw",
      }}
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography component={"span"}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

const dict: any = {
  Hips: 0,
  Abs: 1,
  Buttocks: 2,
  Chest: 3,
  Shoulders: 4,
  Back: 5,
  Legs: 6,
  Arms: 7,
};

export const PersonalCustomizationContainer: React.FC = () => {
  const [value, setValue] = useState(0);
  const [musclesGroups, setMusclesGroups] = useState<IMuscleGroup[]>([]);
  const history = useHistory();
  const { addToast } = useToasts();
  const dataParams: any = useParams();

  useEffect(() => {
    const fetchMusclesGroups = async () => {
      await GetFetchAPI(`/api/muscleGroups`)
        .then((data) => {
          setMusclesGroups(data);
          //history.push("/trainings/personalCustomize/Hips");
          if (dataParams.muscle) {
            history.push(`/trainings/personalCustomize/${dataParams.muscle}`);
            setValue(dict[dataParams.muscle]);
          } else {
            history.push("/trainings/personalCustomize/Hips");
          }
        })
        .catch((error) => {
          addToast(error.toString(), {
            appearance: "error",
            autoDismiss: true,
          });
          history.push("/login");
        });
    };
    fetchMusclesGroups();
  }, []);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div className="personalCustomizationRoot">
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className="tabs"
        classes={{ indicator: "indicator" }}
      >
        {musclesGroups.map((musclesGroup: IMuscleGroup, index: number) => {
          return (
            <Tab
              classes={{ wrapper: "tabColor" }}
              label={musclesGroup.name}
              {...a11yProps(index)}
              key={index}
              component={Link}
              to={"/trainings/personalCustomize/" + musclesGroup.name}
            />
          );
        })}
      </Tabs>
      {dataParams.muscle && (
        <TabPanel value={0} index={0} key={0}>
          <MusclesExecerisesContainer muscle={dataParams.muscle} />
        </TabPanel>
      )}
    </div>
  );
};

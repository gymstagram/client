import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import { useToasts } from "react-toast-notifications";
import FormControl from "@material-ui/core/FormControl";
import { Select, TextField } from "@material-ui/core";
import { IExercise } from "gymstagram-common";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { IFormInput } from "./addTrianingInterface";
import "./muscleExerices.scss";

interface Props {
  addExericse: (exercise: IExercise, personalPreferences: IFormInput) => Promise<boolean>;
  exercise: IExercise;
}

const schema = yup.object().shape({
  day: yup.number().required("Day is required!"),
  sets: yup
    .number()
    .typeError("sets must be number")
    .positive("sets must be a positive number")
    .integer("sets must be am integer")
    .required("sets is required"),
  reps: yup
    .number()
    .typeError("reps must be number")
    .positive("reps must be a positive number")
    .integer("reps must be positive integer")
    .required("reps is required"),
  restTime: yup
    .number()
    .typeError("restTime must be number")
    .positive("rest time must be a positive number")
    .integer("rest timemust be positive integer")
    .required("rest time is required"),
});

export const AddExercise: React.FC<Props> = ({ addExericse, exercise }) => {
  const [open, setOpen] = React.useState(false);
  const { addToast } = useToasts();

  const { control, handleSubmit, errors } = useForm<IFormInput>({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data: IFormInput) => {
    const x = await addExericse(exercise, data);
    if (x) {
      setOpen(false);
      addToast("succefully added exercise!", {
        appearance: "success",
        autoDismiss: true,
      });
    } else {
      addToast("failed added exercise!", {
        appearance: "error",
        autoDismiss: true,
      });
    }
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="primary" onClick={handleClickOpen}>
        Add To My Trainings
      </Button>
      <Dialog
        PaperProps={{
          style: {
            backgroundColor: "#3b393f",
            width: "26rem",
            height: "26rem",
          },
        }}
        disableBackdropClick
        disableEscapeKeyDown
        open={open}
        onClose={handleClose}
      >
        <DialogTitle style={{ color: "#fff" }}>Add exercise to your week</DialogTitle>
        <DialogContent>
          <form className="addTraining__container" onSubmit={handleSubmit(onSubmit)}>
            <FormControl className="addTraining__formControl">
              <Controller
                name="day"
                control={control}
                defaultValue=""
                render={({ onChange, value }) => (
                  <>
                    <InputLabel style={{ color: "#fff" }}>Day</InputLabel>
                    <Select
                      style={{ backgroundColor: /*"#6c757d"*/ "#fff" }}
                      native
                      value={value}
                      onChange={onChange}
                      input={<Input id="demo-dialog-native" />}
                    >
                      <option aria-label="None" value="" />
                      <option value={1}>Sunday</option>
                      <option value={2}>Monday</option>
                      <option value={3}>Tuesday</option>
                      <option value={4}>Wendsday</option>
                      <option value={5}>Thursday</option>
                      <option value={6}>Friday</option>
                    </Select>
                    <p>{errors.day?.message}</p>
                  </>
                )}
              />

              <Controller
                name="sets"
                control={control}
                defaultValue=""
                render={({ onChange, value }) => (
                  <>
                    <TextField
                      style={{ backgroundColor: /*"#6c757d"*/ "#fff" }}
                      placeholder="sets"
                      onChange={onChange}
                      value={value}
                    />
                    <p style={{ color: "#fff" }}>{errors.sets?.message}</p>
                  </>
                )}
              />

              <Controller
                name="reps"
                control={control}
                defaultValue=""
                render={({ onChange, value }) => (
                  <>
                    <TextField
                      style={{ backgroundColor: /*"#6c757d"*/ "#fff" }}
                      placeholder="reps"
                      onChange={onChange}
                      value={value}
                    />
                    <p style={{ color: "#fff" }}>{errors.reps?.message}</p>
                  </>
                )}
              />

              <Controller
                name="restTime"
                control={control}
                defaultValue=""
                render={({ onChange, value }) => (
                  <>
                    <TextField
                      style={{ backgroundColor: /*"#6c757d"*/ "#fff" }}
                      placeholder="rest time"
                      onChange={onChange}
                      value={value}
                    />
                    <p style={{ color: "#fff" }}>{errors.restTime?.message}</p>
                  </>
                )}
              />
            </FormControl>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit(onSubmit)} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

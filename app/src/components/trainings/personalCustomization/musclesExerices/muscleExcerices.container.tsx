import React, { useEffect, useState } from "react";
import "./muscleExerices.scss";
import { AddExercise } from "./dialogAddTraining";
import { GetFetchAPI, PostFetchAPI } from "../../../../services/fetchAPI";
import { useSelector } from "react-redux";
import { usersSelector } from "../../../../slices/users/usersSlice";
import { IExercise } from "gymstagram-common";
import { IFormInput } from "./addTrianingInterface";
import { ExerciseDescription } from "./exercisesDescription";
import { useHistory, useParams } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import { Box, Typography } from "@material-ui/core";

interface Props {
  muscle: string;
}

export const MusclesExecerisesContainer: React.FC<Props> = ({ muscle }) => {
  const [exercises, setMuscleExercises] = useState<IExercise[]>([]);
  const { username } = useSelector(usersSelector);
  const history = useHistory();
  const { addToast } = useToasts();

  useEffect(() => {
    const fetchTrainings = async () => {
      await GetFetchAPI(`/api/exercises/muscleGroup/${muscle}`)
        .then((data) => {
          setMuscleExercises(data);
        })
        .catch((error) => {
          addToast(error.toString(), {
            appearance: "error",
            autoDismiss: true,
          });
          history.push("/login");
        });
    };
    fetchTrainings();
  }, [muscle]);

  const addExericse = async (exercise: IExercise, personalPreferences: IFormInput): Promise<any> => {
    return await PostFetchAPI({ exerciseID: exercise._id, personalPreferences }, `/api/users/${username}/trainings`)
      .then((data) => {
        return data.n >= 1;
      })
      .catch((error) => {
        addToast(error.toString(), {
          appearance: "error",
          autoDismiss: true,
        });
        return false;
      });
  };

  return (
    <div>
      {exercises.map((exercise, index) => {
        return (
          <div className="excercise" key={index}>
            <ExerciseDescription exercise={exercise} />
            <div className="excercise__button">
              <AddExercise exercise={exercise} addExericse={addExericse} />
            </div>
          </div>
        );
      })}
    </div>
  );
};

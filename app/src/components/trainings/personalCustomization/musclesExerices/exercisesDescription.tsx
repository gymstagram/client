import React from "react";
import ReactPlayer from "react-player";
import "./muscleExerices.scss";
import { IExercise, Difficulty } from "gymstagram-common";
import { MuscleDetails } from "./dialogMuscleDetails";
import { VideoPlayer } from "../../../shared/VideoPlayer";

interface Props {
  exercise: IExercise;
}

export const ExerciseDescription: React.FC<Props> = ({ exercise }) => {
  return (
    <div className="excercise__details" key={exercise._id}>
      <div className="excercise__details__name">{exercise.name}</div>
      <div className="excercise__details__picVideo">
        <div>
          <img src={exercise.image} alt={""} height={280} width={248} />
        </div>
        <div className="excercise__details__player">
          <VideoPlayer videoURL={exercise.video} />
          {/* <ReactPlayer url={exercise.video} width="100%" height="100%" /> */}
        </div>
      </div>

      <div className="excercise__details__instruction">
        <span className="excercise__details__instruction__header">instructions:</span>
        <div className="excercise__details__instruction__details">
          {exercise.instructions.map((instruction, index) => {
            return <span className="excercise__details__instruction__details__pointer">👉 {instruction}</span>;
          })}
        </div>
      </div>
      <div className="excercise__details__muscles">
        <div className="excercise__details__muscles__primary">
          <span className="excercise__details__muscles__primary__header">Primary Muscles:</span>
          <div className="excercise__details__muscles__primary__muscles">
            {exercise.muscles.primary.map((muscle, index) => {
              return (
                <span className="excercise__details__muscles__primary__muscles__each">
                  <MuscleDetails muscle={muscle} />
                </span>
              );
            })}
          </div>
        </div>

        <div className="excercise__details__muscles__secondary">
          <span className="excercise__details__muscles__secondary__header">Secondary Muscles:</span>
          <div className="excercise__details__muscles__secondary__muscles">
            {exercise.muscles.secondary.map((muscle, index) => {
              return (
                <span className="excercise__details__muscles__secondary__muscles__each">
                  <MuscleDetails muscle={muscle} />
                </span>
              );
            })}
          </div>
        </div>
        <div className="excercise__diffcultLevel">Difficult Level: {Difficulty[exercise.difficultyLevel]}</div>
      </div>
    </div>
  );
};

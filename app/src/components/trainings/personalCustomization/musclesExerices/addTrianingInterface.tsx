export interface IFormInput {
    day: number;
    reps: number;
    sets: number;
    restTime: number
  }
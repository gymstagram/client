import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { IMuscle } from "gymstagram-common";
import { CircularProgress } from "@material-ui/core";
import "./muscleExerices.scss";

interface Props {
  muscle: IMuscle;
}

export const MuscleDetails: React.FC<Props> = ({ muscle }) => {
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <span onClick={handleClickOpen}>{muscle.name}</span>
      <Dialog
        PaperProps={{
          style: {
            backgroundColor: "#3b393f",
          },
        }}
        disableBackdropClick
        disableEscapeKeyDown
        open={open}
        onClose={handleClose}
      >
        <DialogTitle className="muslceDecription__header">{muscle.name}</DialogTitle>
        <DialogContent>
          <div className="muslceDecription__content">
            <div>{muscle.description}</div>
            <div>
              <>
                <div
                  style={{
                    display: loading ? "block" : "none",
                  }}
                >
                  <CircularProgress style={{ marginLeft: "15rem", marginTop: "3rem" }} />
                </div>
                <div style={{ display: loading ? "none" : "block" }}>
                  <img key={muscle.image} src={muscle.image} onLoad={() => setLoading(false)} />
                </div>
              </>

              {/* {(imageToDisplay = !isLoaded ? <CircularProgress /> : null)} */}
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

import { createStyles, fade, makeStyles, Theme } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import { ITraining } from "gymstagram-common";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import { TRAININGS_SORTED_BY_END_POINTS } from "../../consts/endpoints";
import { GetFetchAPI } from "../../services/fetchAPI";
import { Training } from "./Training";
import "./training.scss";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
    },
    search: {
      position: "relative",
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),

      "&:hover": {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,

      width: "100%",
      [theme.breakpoints.up("sm")]: {
        marginLeft: theme.spacing(3),
        width: "auto",
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    inputRoot: {
      color: "inherit",
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),

      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("md")]: {
        width: "20ch",
      },
    },
  })
);

interface TrainingSortContainerProps {
  sortBy: string;
}
export const TrainingSortContainer: React.FC<TrainingSortContainerProps> = (props) => {
  const classes = useStyles();
  const sortBy = props.sortBy;
  const likes = props;
  const [trainings, setTrainings] = useState<ITraining[]>([]);
  const [searchField, setSearchField] = useState<string>("");
  const history = useHistory();
  const { addToast } = useToasts();

  const onSearchFieldChange = async (event: { target: { value: React.SetStateAction<string> } }) => {
    setSearchField(event.target.value);
  };

  useEffect(() => {
    const updateTraining = async () => {
      if (searchField.length >= 3 || searchField.length == 0) {
        await GetFetchAPI(TRAININGS_SORTED_BY_END_POINTS + "/" + props.sortBy + "?name=" + searchField).then((data) =>
          setTrainings(data)
        );
      }
    };
    updateTraining();
  }, [searchField]);

  useEffect(() => {
    async function getTrainings() {
      console.log("fethcing...");
      GetFetchAPI(TRAININGS_SORTED_BY_END_POINTS + "/" + props.sortBy)
        .then((data) => {
          setTrainings(data);
        })
        .catch((error) => {
          addToast(error.toString(), {
            appearance: "error",
            autoDismiss: true,
          });
          history.push("/login");
        });
    }

    getTrainings();
  }, [props.sortBy]);
  return (
    <div className="trainingContainer">
      <div className={"search"}>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            inputProps={{ "aria-label": "search" }}
            onChange={onSearchFieldChange}
          />
        </div>
      </div>

      {trainings.map((training: ITraining, index: number) => {
        return (
          <div key={index}>
            <Card className="customTraining" children={<Training training={training} />} />
          </div>
        );
      })}
    </div>
  );
};

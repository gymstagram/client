import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { IUserExercise } from "gymstagram-common";
import { ExerciseDescription } from "../personalCustomization/musclesExerices/exercisesDescription";
import "./myTrainings.scss";

interface Props {
  userExercise: IUserExercise;
}

export const ExerciseDetails: React.FC<Props> = ({ userExercise }) => {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <span className="spanHover" onClick={handleClickOpen}>
        👉 <b>{userExercise.exercise.name}</b>: {userExercise.sets} sets, {userExercise.reps} reps,{" "}
        {userExercise.restTime} seconds rest
      </span>
      <Dialog
        classes={{ paperWidthSm: "paperWidthSm" }}
        disableBackdropClick
        disableEscapeKeyDown
        open={open}
        onClose={handleClose}
      >
        <DialogContent>
          <ExerciseDescription exercise={userExercise.exercise} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

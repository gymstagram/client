import React, { useEffect, useState } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { GetFetchAPI, DeleteFetchAPI } from "../../../services/fetchAPI";
import { useSelector } from "react-redux";
import { usersSelector } from "../../../slices/users/usersSlice";
import { IDailyTraining, IMuscleGroup, IUserExercise } from "gymstagram-common";
import { ExerciseDetails } from "./exerciseDescriptionModal";
import { useHistory, useParams } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import { Box, Button } from "@material-ui/core";

interface IDailyTraining2 extends IDailyTraining {
  muscleGroup: IMuscleGroup;
}

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      style={{ height: "80vh", overflowY: "auto" }}
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography component={"span"}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export const TrainingExecerisesContainer: React.FC = () => {
  const [dayExercises, setDayExercises] = useState<IDailyTraining2[]>([]);
  const { username } = useSelector(usersSelector);
  const history = useHistory();
  const data: any = useParams();
  const { addToast } = useToasts();

  useEffect(() => {
    const fetchTrainings = async () => {
      // in server side need to do group by tag in exerices
      await GetFetchAPI(`/api/users/${username}/trainings/${data.day}`)
        .then((data) => {
          setDayExercises(data);
        })
        .catch((error) => {
          addToast(error.toString(), {
            appearance: "error",
            autoDismiss: true,
          });
          history.push("/login");
        });
    };
    fetchTrainings();
  }, [data.day]);

  const handleDeleteExcercise = async (userExercise: IUserExercise) => {
    await DeleteFetchAPI(`/api/users/${username}/trainings/${data.day}/${userExercise.exercise._id}`)
      .then(() => {
        dayExercises.forEach((dayExercise, index) => {
          dayExercise.exercises = dayExercise.exercises.filter(
            (exercise) => exercise.exercise._id !== userExercise.exercise._id
          );
        });
        setDayExercises(dayExercises.filter((dayExercise) => dayExercise.exercises.length > 0));
        addToast("Delete Excercise", {
          appearance: "success",
          autoDismiss: true,
        });
      })
      .catch((error) => {
        addToast(error.toString(), {
          appearance: "error",
          autoDismiss: true,
        });
      });
  };

  return (
    <TabPanel value={0} index={0} key={0}>
      <div className="accordionsRoot">
        {dayExercises.map((dayExercise, index) => {
          return (
            <Accordion className="accordionsRoot__custom">
              <AccordionSummary
                classes={{ content: "accordionsRoot__custom__header" }}
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography component={"span"} className="accordionsRoot__custom__heading">
                  {dayExercise.muscleGroup}
                </Typography>
                {/* <Button style={{ backgroundColor: "#ff000066", color: "#fff" }} onClick={handleDeleteExcercises}>
                delete
              </Button> */}
              </AccordionSummary>
              <AccordionDetails classes={{ root: "accordionsRoot__custom__details" }}>
                {dayExercise.exercises.map((userExercise: IUserExercise, index) => {
                  return (
                    <div className="accordionsRoot__custom__details__exercise">
                      <ExerciseDetails userExercise={userExercise} />
                      <Button
                        style={{ backgroundColor: "#ff000066", color: "#fff" }}
                        onClick={() => handleDeleteExcercise(userExercise)}
                      >
                        remove
                      </Button>
                    </div>
                  );
                })}
              </AccordionDetails>
            </Accordion>
          );
        })}
      </div>
    </TabPanel>
  );
};

import React, { useEffect } from "react";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import "./myTrainings.scss";
import { Link, useHistory, useLocation, useParams } from "react-router-dom";

function a11yProps(index: any) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

export const MyTrainingsContainer: React.FC = () => {
  const [value, setValue] = React.useState(0);
  const history = useHistory();
  const location: any = useLocation();
  const dataParams: any = useParams();

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  useEffect(() => {
    if (!dataParams.day) {
      history.push("/trainings/myTraining/1");
    } else {
      if (location.pathname.includes("1")) {
        setValue(0);
        history.push(location.pathname);
      } else {
        if (location.pathname.includes("2")) {
          setValue(1);
          history.push(location.pathname);
        } else {
          if (location.pathname.includes("3")) {
            setValue(2);
            history.push(location.pathname);
          } else {
            if (location.pathname.includes("4")) {
              setValue(3);
              history.push(location.pathname);
            } else {
              if (location.pathname.includes("5")) {
                setValue(4);
                history.push(location.pathname);
              } else {
                if (location.pathname.includes("6")) {
                  setValue(5);
                  history.push(location.pathname);
                } else {
                  setValue(0);
                  history.push(location.pathname);
                }
              }
            }
          }
        }
      }
    }
  }, []);

  return (
    <div className="myTrainingsRoot">
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
          classes={{ flexContainer: "myTrainingsFlexContainer" }}
        >
          <Tab
            classes={{ wrapper: "tabColor" }}
            label="Sunday"
            {...a11yProps(0)}
            component={Link}
            to={"/trainings/myTraining/1"}
          />
          <Tab
            classes={{ wrapper: "tabColor" }}
            label="Monday"
            {...a11yProps(1)}
            component={Link}
            to={"/trainings/myTraining/2"}
          />
          <Tab
            classes={{ wrapper: "tabColor" }}
            label="Tuesday"
            {...a11yProps(2)}
            component={Link}
            to={"/trainings/myTraining/3"}
          />
          <Tab
            classes={{ wrapper: "tabColor" }}
            label="Wendsday"
            {...a11yProps(3)}
            component={Link}
            to={"/trainings/myTraining/4"}
          />
          <Tab
            classes={{ wrapper: "tabColor" }}
            label="Thursday"
            {...a11yProps(4)}
            component={Link}
            to={"/trainings/myTraining/5"}
          />
          <Tab
            classes={{ wrapper: "tabColor" }}
            label="Friday"
            {...a11yProps(5)}
            component={Link}
            to={"/trainings/myTraining/6"}
          />
        </Tabs>
      </AppBar>
    </div>
  );
};

import { Button, Typography } from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import { IPost, ITraining } from "gymstagram-common";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useToasts } from "react-toast-notifications";
import { USERS_ENDPOINT } from "../consts/endpoints";
import { PostFetchAPI } from "../services/fetchAPI";
import "./utils.scss";

type ILike = { object: ITraining | IPost; type: string };

export const Likes: React.FC<ILike> = (props) => {
  const { _id } = { ...props.object };
  const type = props.type;
  let logedInUser = "damogi";
  const { likedBy, numOfLikes } = { ...props.object };
  const [likes, setLikes] = useState(0);
  const [likedByCurrentUser, setLikedByCurrentUser] = useState(false);
  const { addToast } = useToasts();
  const history = useHistory();
  useEffect(() => {
    const doesLikedByCurrentUser = likedBy!.includes(logedInUser!);
    setLikedByCurrentUser(doesLikedByCurrentUser);
    setLikes(props.object.numOfLikes);
  }, [props.object]);

  const handleLike = async () => {
    let endPoint = `${USERS_ENDPOINT}/like`;
    // type == "training"
    //   ? `${TRAININGS_ENDPOINT}/like`
    //   : `${EXERCISE_ENDPOINT}/like`;
    let likesToChange = likedByCurrentUser ? -1 : 1;

    await PostFetchAPI({ objectId: _id, user: logedInUser, likes: likesToChange, type: type }, endPoint)
      .then(() => {
        if (likedByCurrentUser) {
          setLikes(likes - 1);
        } else {
          setLikes(likes + 1);
        }
        setLikedByCurrentUser(!likedByCurrentUser);
      })
      .catch((error) => {
        addToast(error.toString(), {
          appearance: "error",
          autoDismiss: true,
        });
        history.push("/login");
      });
  };
  return (
    <div className="likes">
      <Button className="likes__button" onClick={handleLike}>
        {likedByCurrentUser ? <FavoriteIcon color="secondary" /> : <FavoriteBorderIcon color="secondary" />}
      </Button>
      <Typography component={"span"} className="likes__color">
        {likes} Likes
      </Typography>
    </div>
  );
};

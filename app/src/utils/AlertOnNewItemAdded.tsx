import React, { Component, useEffect, useState } from "react";
import { Alert } from "@material-ui/lab";
import { io } from "socket.io-client";
import "../components/explore/posts/explore.scss";
export const AlertOnNewItemAdded = () => {
  const [alertType, setAlertType] = useState<string>("");
  const [showAlert, setShowAlert] = useState<boolean>(false);
  useEffect(() => {
    console.log("connectiong to socket server");
    const socket = io(process.env.REACT_APP_SERVER_HOST!);

    socket.on("newItem", (itemType: string) => {
      console.log(`new item of type ${itemType} has added`);
      setAlertType(itemType);
      setShowAlert(true);
      window.setTimeout(() => {
        setShowAlert(false);
      }, 3000);
    });
  }, []);
  return (
    <>
      {showAlert ? (
        <Alert variant="filled" severity="success" className={"alert"}>
          A new {alertType} has added , check it out !
        </Alert>
      ) : null}
    </>
  );
};

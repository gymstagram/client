export interface User {
    username: string | undefined,
    accessToken: string | undefined
}
import { AppThunk, AppDispatch } from "../configureStore";
import { GetFetchAPI, PostFetchAPI } from "../../services/fetchAPI";
import { usersActions } from "./usersSlice";
import { IExercise, ITraining } from "gymstagram-common";

export const logingUser = (username: string, password: string): AppThunk => {
  return async (dispatch: AppDispatch) => {
    const user = await PostFetchAPI({ username, password }, `/api/users/login`);
    if (!user) {
      throw Error("There is a problem, try later!");
    }
    dispatch(usersActions.saveUser(user));
  };
};

export const signUpUser = (username: string, password: string): AppThunk => {
  return async (dispatch: AppDispatch) => {
    await PostFetchAPI({ username, password }, `/api/users/signup`);
  };
};

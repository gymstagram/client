import { createSlice, PayloadAction  } from '@reduxjs/toolkit'
import {User} from "./interfaces"
import {loadState} from "../localStroge"

export const initialState: User = {
  username: loadState("username"),
  accessToken: loadState("accessToken")
}

const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    saveUser: (state, action: PayloadAction<User>) => {
     state.username = action.payload.username
     state.accessToken = action.payload.accessToken
    }
  },
})

export const usersActions = usersSlice.actions;
export const usersSelector = (state: { users: any }) => state.users

export default usersSlice.reducer
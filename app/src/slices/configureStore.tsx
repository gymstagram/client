import { configureStore, Action  } from '@reduxjs/toolkit'
import rootReducer, { RootState } from "./rootReducer"
import { ThunkAction } from 'redux-thunk'
import {saveState} from "../slices/localStroge"
import { StarRounded } from '@material-ui/icons'

const store = configureStore({
    reducer: rootReducer 
})

store.subscribe(() => {
    saveState(store.getState().users.accessToken, "accessToken")
    saveState(store.getState().users.username, "username")
})

export type AppDispatch = typeof store.dispatch
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>

export default store
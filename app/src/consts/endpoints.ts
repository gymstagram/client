export let POSTS_ENDPOINT = `/api/posts`;
export let TRAININGS_ENDPOINT = `/api/trainings`;
export let EXERCISE_ENDPOINT = `/api/exercises`;
export let TRAININGS_SORTED_BY_END_POINTS = `/api/trainings/sortedTraining`;
export let USERS_ENDPOINT = "/api/users";

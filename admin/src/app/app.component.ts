import { Component, Injector, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { AuthService } from "./modules/auth/services/auth.service";
import { LoaderService } from "./modules/core/services/loader.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
})
export class AppComponent implements OnInit {
  isLoading$: Observable<boolean>;
  isAuth$: Observable<boolean>;
  constructor(private loaderService: LoaderService, private injector: Injector, private authService: AuthService) {
    ServiceLocator = this.injector;
  }
  ngOnInit(): void {
    this.isLoading$ = this.loaderService.isLoading;
    this.isAuth$ = this.authService.getAuthenticatedUser();
  }
}
export let ServiceLocator: Injector;

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginActivate } from "./modules/auth/can-activate.guard";
import { LoginComponent } from "./modules/auth/login/login.component";

const routes: Routes = [
  { path: "", redirectTo: "exercises", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  {
    path: "graphs",
    loadChildren: () => import("./modules/graphs/graphs.module").then((m) => m.GraphsModule),
    canActivate: [LoginActivate],
  },
  { path: "*", redirectTo: "exercises", pathMatch: "full" },
  {
    path: "exercises",
    loadChildren: () => import("./modules/exercise/exercise.module").then((m) => m.ExerciseModule),
    canActivate: [LoginActivate],
  },
  {
    path: "trainings",
    loadChildren: () => import("./modules/training/training.module").then((m) => m.TrainingModule),
    canActivate: [LoginActivate],
  },
  { path: "**", redirectTo: "login" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

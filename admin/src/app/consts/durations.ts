export const durations = [
  { name: "Max 10 minutes long", duration: 600 },
  { name: "Max 20 minutes long", duration: 1200 },
  { name: "Max 30 minutes long", duration: 1800 },
  { name: "Max 1 hour long", duration: 3600 },
];

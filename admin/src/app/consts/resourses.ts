export enum Resourse {
  exercise = "exercise",
  training = "training",
  tag = "tag",
  muscle = "muscle",
  post = "post",
  muscleGroup = "muscleGroup",
}
export enum ResourseApi {
  exercise = "exercises",
  training = "trainings",
  tag = "tags",
  muscle = "muscles",
  post = "posts",
  muscleGroup = "muscleGroups",
}

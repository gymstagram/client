export enum Filters {
  name = "name",
  muscleGroup = "muscleGroup",
  difficultyLevel = "difficultyLevel",
  muscles = "muscles",
}

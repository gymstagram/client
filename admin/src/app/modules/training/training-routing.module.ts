import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TrainingContainerComponent } from "./components/training-container/training-container.component";

const routes: Routes = [{ path: "", component: TrainingContainerComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrainingRoutingModule {}

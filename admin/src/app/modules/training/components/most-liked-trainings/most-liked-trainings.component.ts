import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { IPost } from "gymstagram-common";
import { Observable } from "rxjs";
import { LoaderService } from "src/app/modules/core/services/loader.service";
import { PostsService } from "src/app/modules/core/services/posts.service";

@Component({
  selector: "app-most-liked-trainings",
  templateUrl: "./most-liked-trainings.component.html",
  styleUrls: ["./most-liked-trainings.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MostLikedTrainingsComponent implements OnInit {
  constructor(private postsService: PostsService, private loaderService: LoaderService) {}
  posts$: Observable<IPost>;
  isLoading$: Observable<boolean>;
  data$: Observable<any>;

  async ngOnInit() {
    this.isLoading$ = this.loaderService.isLoading;
    this.data$ = this.postsService.getMostLikedPosts();
  }
}

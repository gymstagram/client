import { Component, OnInit } from "@angular/core";
import { IMuscleGroup } from "gymstagram-common";
import { PrimeNGConfig } from "primeng/api";
import { Observable, of } from "rxjs";
import { difficulties, durations, Resourse } from "src/app/consts";
import { EntityModalActionsService } from "src/app/modules/core/services/entity-modal-actions.service";
import { EntitySearchService } from "src/app/modules/core/services/entity-search.service";
import { MuscleGroupService } from "src/app/modules/core/services/muscle-group.service";
import { FilteredSelect } from "src/app/modules/shared/filtered-search/filtered-search.component";
import { TrainingService } from "src/app/modules/training/services/training.service";
import { PageResult } from "../../../shared/entity-display/entity-display.component";
import { TrainingFormComponent } from "../training-form/training-form.component";
import { TrainingInfoDisplayComponent } from "../training-info-display/training-info-display.component";

@Component({
  selector: "app-training-container",
  templateUrl: "./training-container.component.html",
  styleUrls: ["./training-container.component.scss"],
  providers: [
    EntityModalActionsService,
    {
      provide: "modalData",
      useValue: {
        resourse: Resourse.training,
        infoModal: TrainingInfoDisplayComponent,
        mutationForm: TrainingFormComponent,
      },
    },
  ],
})
export class TrainingContainerComponent implements OnInit {
  trainingsPage$: Observable<PageResult>;
  Resourse = Resourse;
  muscleGroups$: Observable<IMuscleGroup[]>;
  filteredSearches: FilteredSelect[] = [];
  constructor(
    private primengConfig: PrimeNGConfig,
    private trainingService: TrainingService,
    private entitySearchService: EntitySearchService,
    private entityModalActionService: EntityModalActionsService,
    private muscleGroupService: MuscleGroupService
  ) {}

  ngOnInit(): void {
    this.trainingsPage$ = this.trainingService.getLatestPage();
    this.muscleGroups$ = this.muscleGroupService.fetchAll();
    this.fetchNextPage({ pageNumber: 1 });
    this.primengConfig.ripple = true;
    this.setFilteredSearches();
  }

  setFilteredSearches() {
    // initialize the filtered search
    const muscleGroupSearch: FilteredSelect = {
      options$: this.muscleGroups$,
      placeholder: "Select Muscle Group",
      selectionLimit: 1,
      name: "musclesGroups",
      showHeader: true,
      searchUrlBy: "_id",
    };

    const difficultySearch: FilteredSelect = {
      options$: of(difficulties),
      placeholder: "Difficulty Level",
      selectionLimit: 1,
      name: "difficultyLevel",
      showHeader: false,
      searchUrlBy: "level",
    };

    const durationsSearch: FilteredSelect = {
      options$: of(durations),
      placeholder: "Training Duration",
      selectionLimit: 1,
      name: "duration",
      showHeader: false,
      searchUrlBy: "duration",
    };
    this.filteredSearches.push(muscleGroupSearch, difficultySearch, durationsSearch);
  }

  deleteTraining(training: any) {
    this.entityModalActionService.deleteWithConsent(training);
  }

  displayTrainingInfo(training: any) {
    this.entityModalActionService.displayInfo(training);
  }

  editTraining(training: any) {
    this.entityModalActionService.editEntity(training);
  }

  addTraining() {
    this.entityModalActionService.addEntity();
  }

  fetchNextPage(page: any) {
    const currentFilter = this.trainingService.getFilter();
    this.trainingService.setSearchFilter({ ...currentFilter, pageNumber: page.pageNumber }).fetchWithFilter();
  }
}

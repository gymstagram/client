import { ChangeDetectorRef, Component, OnDestroy, OnInit } from "@angular/core";
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ITraining } from "gymstagram-common";
import { cloneDeep } from "lodash";
import { MessageService, PrimeNGConfig } from "primeng/api";
import { DynamicDialogConfig } from "primeng/dynamicdialog";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { difficulties } from "src/app/consts/training";
import { LoaderService } from "src/app/modules/core/services/loader.service";
import { PopupService } from "src/app/modules/core/services/popup.service";
import { ExerciseService } from "src/app/modules/exercise/services/exercise.service";
import { IGroupExercises, TrainingService } from "src/app/modules/training/services/training.service";
import { ValidationService } from "../../../core/validations/validation.service";

@Component({
  selector: "app-training-form",
  templateUrl: "./training-form.component.html",
  styleUrls: ["./training-form.component.scss"],
})
export class TrainingFormComponent implements OnInit, OnDestroy {
  difficulties = difficulties.map((x) => ({ name: x.name, value: x.level }));
  trainingForm: FormGroup;
  destroyer$: Subject<void> = new Subject();
  muscleGroupExercises: FormArray;
  populatedForEdit: boolean = false;
  mgExercises: IGroupExercises[] = [];
  trainingToEdit: ITraining;
  formLoaded: boolean;
  formMode: string;
  isLoading$: Observable<boolean>;
  openedModalData: any;
  constructor(
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    private exerciseService: ExerciseService,
    private trainingService: TrainingService,
    private fb: FormBuilder,
    public config: DynamicDialogConfig,
    private popupService: PopupService,
    private loadingService: LoaderService,
    private validationService: ValidationService
  ) {
    this.openedModalData = this.config.data;
    this.formMode = this.openedModalData.edit ? "Edit" : "Add";
    this.trainingToEdit = this.openedModalData.entityToEdit as ITraining;
    this.trainingForm = this.getFormDefaultValues();
    this.muscleGroupExercises = this.trainingForm.get("muscleGroupExercises") as FormArray;
  }

  ngOnInit(): void {
    // this.cd.reattach();
    this.primengConfig.ripple = true;
    this.isLoading$ = this.loadingService.isLoading;

    this.exerciseService.getExercisesByGroups().subscribe((ebg) => {
      this.mgExercises = cloneDeep(ebg);
      this.populateForm();
      this.formLoaded = true;
    });

    // this.muscleGroups$ = this.muscleGroupService.fetchAll();
  }

  validatemuscleGroupExercises(control: AbstractControl): { [key: string]: any } | null {
    if (Array.from(control.value).every((mge: any) => !mge.exercises.length)) {
      return { required: true };
    }
    return null;
  }

  getFormDefaultValues() {
    return this.fb.group({
      name: [null, Validators.required],
      video: [
        null,
        [Validators.required, Validators.pattern(new RegExp("^(https?://)?(www.youtube.com|youtu.?be)/.+$"))],
      ],
      muscleGroupExercises: this.fb.array([], this.validatemuscleGroupExercises),
      difficultyLevel: [null, Validators.required],
    });
  }
  addMuscleGroupExercises() {
    this.mgExercises.forEach((mg) => {
      this.muscleGroupExercises.push(this.createMuscleGroup({ group: mg.group, exercises: [] }));
    });

    if (this.formMode === "Edit") {
      // convert training to {group,exercises} array;
      const exerciseByGroups = this.trainingService.getTrainingExercisesByGroups(this.trainingToEdit);
      exerciseByGroups.map((muscleGroup) => {
        this.muscleGroupExercises.controls.map((formGroup, index) => {
          if (formGroup.get("group")?.value === muscleGroup.group) {
            const exercises = muscleGroup.exercises.map((ex) => ({
              name: ex.name,
              _id: ex._id,
              muscleGroup: ex.muscleGroup,
            }));
            formGroup.patchValue({ exercises });
          }
        });
      });
    }
  }

  createMuscleGroup(value: { group: string; exercises: { name: string }[] }) {
    const { group, exercises } = value;
    return this.fb.group({
      group,
      exercises: [exercises],
    });
  }

  populateForm() {
    if (this.openedModalData.edit) {
      const { name, video, difficultyLevel } = this.trainingToEdit;
      this.trainingForm.patchValue({ name, video, difficultyLevel });
      this.setDifficulty();
    }
    this.addMuscleGroupExercises();
  }

  setDifficulty() {
    this.trainingForm.patchValue({ difficultyLevel: this.difficulties[this.trainingToEdit?.difficultyLevel] });
  }

  onSubmit() {
    if (this.trainingForm.valid) {
    } else {
      return this.validationService.validateAllFormFields(this.trainingForm);
    }
    const newTraining: any = this.trainingService.createTrainingFromForm(this.trainingForm.value);

    if (this.openedModalData.edit) {
      const trainingId = this.trainingToEdit._id;

      this.trainingService
        .update(trainingId!, newTraining)
        .pipe(takeUntil(this.destroyer$))
        .subscribe((res) => {
          this.popupService.showActionMessage(res, "Error in updating training", "Training Edited Successfully!");
        });
    } else {
      this.trainingService
        .create(newTraining)
        .pipe(takeUntil(this.destroyer$))
        .subscribe((res) => {
          this.popupService.showActionMessage(res, "Error in creating training", "Training Created Successfully!");
        });
    }
  }

  ngOnDestroy(): void {
    this.destroyer$.next();
    this.destroyer$.complete();
  }
}

import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { ITraining } from "gymstagram-common";
import { DynamicDialogConfig } from "primeng/dynamicdialog";

@Component({
  selector: "app-training-info-display",
  templateUrl: "./training-info-display.component.html",
  styleUrls: ["./training-info-display.component.scss"],

  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingInfoDisplayComponent implements OnInit {
  constructor(public config: DynamicDialogConfig) {}

  dataToDisplay: ITraining;

  ngOnInit(): void {
    this.dataToDisplay = this.config.data;
  }
}

import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { MostLikedTrainingsComponent } from "./components/most-liked-trainings/most-liked-trainings.component";
import { TrainingContainerComponent } from "./components/training-container/training-container.component";
import { TrainingFormComponent } from "./components/training-form/training-form.component";
import { TrainingInfoDisplayComponent } from "./components/training-info-display/training-info-display.component";
import { ListBoxOptionsPipe } from "./pipes/list-box-options.pipe";
import { MinuteSecondsPipe } from "./pipes/minutes-secs.pipe";
import { TrainingRoutingModule } from "./training-routing.module";

const componets: any = [
  TrainingContainerComponent,
  TrainingFormComponent,
  TrainingInfoDisplayComponent,
  MostLikedTrainingsComponent,
];
const pipes: any = [ListBoxOptionsPipe, MinuteSecondsPipe];

@NgModule({
  declarations: [...componets, ...pipes],
  imports: [TrainingRoutingModule, SharedModule],
  exports: [MostLikedTrainingsComponent],
})
export class TrainingModule {}

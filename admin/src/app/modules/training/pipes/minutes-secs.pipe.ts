import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "minuteSeconds",
})
export class MinuteSecondsPipe implements PipeTransform {
  transform(value: number): string {
    const minutes = Math.floor(value / 60) < 10 ? "0" + Math.floor(value / 60) : Math.floor(value / 60);
    const seconds = value - +minutes * 60 > 10 ? value - +minutes * 60 : value - +minutes * 60 + "0";
    return minutes + ":" + seconds;
  }
}

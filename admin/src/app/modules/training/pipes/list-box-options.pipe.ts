import { Pipe, PipeTransform } from "@angular/core";
import memo from "memo-decorator";
import { IGroupExercises } from "src/app/modules/training/services/training.service";
@Pipe({
  name: "listBoxOptions",
})
export class ListBoxOptionsPipe implements PipeTransform {
  @memo()
  transform(group: string, muscleGroupExercises: IGroupExercises[]) {
    return (
      muscleGroupExercises
        .find((mg) => mg.group === group)
        ?.exercises?.map((ex) => ({
          name: ex.name,
          _id: ex._id,
          muscleGroup: ex.muscleGroup,
        })) || []
    );
  }
}

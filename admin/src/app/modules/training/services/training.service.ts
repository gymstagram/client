import { Injectable } from "@angular/core";
import { IExercise, IMuscle, IMuscleGroup, ITraining } from "gymstagram-common";
import { find } from "lodash";
import { ResourseApi } from "src/app/consts";
import { CrudService } from "../../core/services/generic.crud.service";
import { VideoService } from "../../core/services/video.service";
export interface IGroupExercises {
  group: string;
  exercises: IExercise[];
}

export interface ITrainingForm {
  name: string;
  difficultyLevel: { name: string; value: number };

  muscleGroupExercises: {
    group: string;
    exercises: { name: string; _id: string; muscleGroup: IMuscleGroup }[];
  }[]; // todo change this later
  video: string;
}
@Injectable({
  providedIn: "root",
})
export class TrainingService extends CrudService<ITraining> {
  constructor(private videoService: VideoService) {
    super(ResourseApi.training);
  }
  getTrainingExercisesByGroups(training: ITraining) {
    const map: Map<string, IExercise[]> = new Map();
    const result: IGroupExercises[] = [];
    const exercises = training.exercises;
    exercises.forEach((ex) => {
      map.set(ex.muscleGroup.name, [...(map.get(ex.muscleGroup.name) || []), ex]);
    });
    for (const [group, exercises] of map) {
      result.push({ group, exercises });
    }
    return result;
  }

  createTrainingFromForm(form: ITrainingForm) {
    const { difficultyLevel, name, muscleGroupExercises, video } = form;
    console.log();
    const musclesGroups: Pick<IMuscle, "_id" | "name">[] = [];
    const exercises: Pick<IExercise, "_id" | "name" | "muscleGroup">[] = [];
    const formalizedVideoURL = this.videoService.formalizeYoutubeUrl(video);

    const formSelectedExercises: { name: string; _id: string; muscleGroup: IMuscleGroup }[] = [];
    muscleGroupExercises.forEach((mge) => {
      mge.exercises.map((ex) => formSelectedExercises.push(ex));
    });
    formSelectedExercises.map((te) => {
      const newMuscleGroup = { _id: te.muscleGroup._id, name: te.muscleGroup.name };
      if (!find(musclesGroups, newMuscleGroup)) {
        musclesGroups.push(newMuscleGroup);
      }
      exercises.push({ _id: te._id, name: te.name, muscleGroup: te.muscleGroup });
    });
    return { difficultyLevel: difficultyLevel.value, name, musclesGroups, exercises, video: formalizedVideoURL };
  }
}

import { Injectable } from "@angular/core";
import { IExercise } from "gymstagram-common";
import { of } from "rxjs";
import { switchMap } from "rxjs/operators";
import { ResourseApi } from "src/app/consts";
import { CrudService } from "../../core/services/generic.crud.service";
import { VideoService } from "../../core/services/video.service";
import { IGroupExercises } from "../../training/services/training.service";

@Injectable({
  providedIn: "root",
})
export class ExerciseService extends CrudService<IExercise> {
  exercisesByGroups: IGroupExercises[] = [];
  constructor(private videoService: VideoService) {
    super(ResourseApi.exercise);
  }

  getExercisesByGroups() {
    if (this.exercisesByGroups.length) {
      return of(this.exercisesByGroups);
    }
    return this.fetchOne<IGroupExercises[]>(ResourseApi.muscleGroup).pipe(
      switchMap((res) => {
        this.exercisesByGroups = res;
        return of(this.exercisesByGroups);
      })
    );
  }

  getFormalizedForm(form: IExercise) {
    form.video = this.videoService.formalizeYoutubeUrl(form.video);
    form.difficultyLevel = (form.difficultyLevel as any).value;
    return form;
  }
}

import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { IExercise } from "gymstagram-common";
import { DynamicDialogConfig } from "primeng/dynamicdialog";

@Component({
  selector: "app-exercise-info-display",
  templateUrl: "./exercise-info-display.component.html",
  styleUrls: ["./exercise-info-display.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExerciseInfoDisplayComponent implements OnInit {
  constructor(public config: DynamicDialogConfig) {}

  dataToDisplay: IExercise;
  isLoadingImg: boolean = true;

  ngOnInit(): void {
    this.dataToDisplay = this.config.data;
  }
}

import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { DomSanitizer } from "@angular/platform-browser";
import { IExercise, IMuscle, IMuscleGroup } from "gymstagram-common";
import { cloneDeep } from "lodash";
import { MessageService, PrimeNGConfig } from "primeng/api";
import { DynamicDialogConfig } from "primeng/dynamicdialog";
import { MultiSelect } from "primeng/multiselect";
import { Observable, Subject } from "rxjs";
import { map, takeUntil } from "rxjs/operators";
import { difficulties } from "src/app/consts";
import { MuscleGroupService } from "src/app/modules/core/services/muscle-group.service";
import { MuscleService } from "src/app/modules/core/services/muscle.service";
import { PopupService } from "src/app/modules/core/services/popup.service";
import { ValidationService } from "src/app/modules/core/validations/validation.service";
import { ExerciseService } from "../../services/exercise.service";

interface IMuscleDisplay extends IMuscle {
  inactive: boolean;
}
@Component({
  selector: "app-exercise-form",
  templateUrl: "./exercise-form.component.html",
  styleUrls: ["./exercise-form.scss"],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExerciseFormComponent implements OnInit, AfterViewInit, OnDestroy {
  primaryMuscles: IMuscleDisplay[] = [];
  secondaryMuscles: IMuscleDisplay[] = [];
  subscribedToSelectChanges: boolean = false;
  muscleGroups$: Observable<IMuscleGroup[]>;
  muscles$: Observable<IMuscle[]>;
  exerciseForm: FormGroup;
  destroyer$: Subject<void> = new Subject();
  exerciseInstructions: FormArray;
  populatedForEdit: boolean = false;
  formLoaded: boolean;
  openedModalData: any;
  @ViewChildren(MultiSelect) ms: QueryList<MultiSelect>;
  exerciseToEdit: IExercise;
  difficulties = difficulties.map((x) => ({ name: x.name, value: x.level }));
  formMode: string;
  constructor(
    private primengConfig: PrimeNGConfig,
    private exerciseService: ExerciseService,
    private fb: FormBuilder,
    private popupService: PopupService,
    public config: DynamicDialogConfig,
    private muscleService: MuscleService,
    private validationService: ValidationService,
    private muscleGroupService: MuscleGroupService
  ) {
    this.openedModalData = this.config.data;
    this.formMode = this.openedModalData.edit ? "Edit" : "Add";
    this.exerciseToEdit = this.openedModalData.entityToEdit as IExercise;
    this.exerciseForm = this.getFormDefaultValues();
    this.exerciseInstructions = this.exerciseForm.get("instructions") as FormArray;
  }

  ngAfterViewInit() {
    this.ms.forEach((multiselect) => {
      const originalRemoveChip = multiselect.removeChip;
      multiselect.removeChip = (...args) => {
        originalRemoveChip.apply(multiselect, args);
        multiselect.onModelChange(multiselect.value);
      };
    });
  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.muscleGroups$ = this.muscleGroupService.fetchAll();
    this.muscles$ = this.muscleService.fetchAll();
    this.populateForm();
    this.formLoaded = true;
  }

  getFormDefaultValues() {
    return this.fb.group({
      name: [null, Validators.required],
      video: [
        null,
        [Validators.required, Validators.pattern(new RegExp("^(https?://)?(www.youtube.com|youtu.?be)/.+$"))],
      ],
      muscleGroup: [null, Validators.required],
      muscles: this.fb.group({
        primary: [[], Validators.required],
        secondary: [],
      }),
      image: [null, [Validators.required, Validators.pattern(new RegExp("(.(jpe?g|png|gif|bmp))$"))]],
      difficultyLevel: [null, Validators.required],
      instructions: this.fb.array([], Validators.required),
    });
  }

  addInstruction(value: string | null = null) {
    this.exerciseInstructions.push(this.createInstruction(value));
  }

  createInstruction(value: string | null = null) {
    return new FormControl(value, Validators.required);
  }
  deleteInstruction() {
    this.exerciseForm.markAsDirty();
    this.exerciseInstructions.removeAt(this.exerciseInstructions.length - 1);
  }
  populateForm() {
    if (this.openedModalData.edit) {
      this.exerciseForm.patchValue(this.exerciseToEdit);
      this.setDifficulty();
    }
    this.setMuscles();
    this.setInstructions();
  }

  setDifficulty() {
    this.exerciseForm.patchValue({ difficultyLevel: this.difficulties[this.exerciseToEdit?.difficultyLevel] });
  }
  setMuscles() {
    this.muscles$
      .pipe(
        takeUntil(this.destroyer$),
        map((res) => {
          const receivedMuscles = res;
          const mutableMuscleArray: IMuscleDisplay[] = [];
          for (const muscle of receivedMuscles) {
            mutableMuscleArray.push({ ...muscle, inactive: false });
          }
          this.primaryMuscles = cloneDeep(mutableMuscleArray);
          this.secondaryMuscles = cloneDeep(mutableMuscleArray);
          this.subscribeToSelectValueChanges();
          this.updateMusclesFormField();
        })
      )
      .subscribe();
  }

  setInstructions() {
    if (this.formMode === "Add") {
      this.addInstruction();
    } else {
      this.exerciseToEdit.instructions.map((instruction) => this.addInstruction(instruction));
    }
  }

  subscribeToSelectValueChanges() {
    if (!this.subscribedToSelectChanges) {
      this.subscribedToSelectChanges = true;
      (this.exerciseForm.controls.muscles as FormGroup).controls.primary.valueChanges
        .pipe(takeUntil(this.destroyer$))
        .subscribe((value) => {
          this.changeIfSelectionPossible(value, "primary");
        });

      (this.exerciseForm.controls.muscles as FormGroup).controls.secondary.valueChanges
        .pipe(takeUntil(this.destroyer$))
        .subscribe((value) => {
          this.changeIfSelectionPossible(value, "secondary");
        });
    }
  }

  updateMusclesFormField() {
    if (!this.populatedForEdit && this.openedModalData.edit) {
      this.populatedForEdit = true;
      const exercisePrimaryMuscles = this.exerciseToEdit.muscles.primary.map((muscle) => ({
        ...muscle,
        inactive: false,
      }));
      const exerciseSecondaryMuscles = this.exerciseToEdit.muscles.secondary.map((muscle) => ({
        ...muscle,
        inactive: false,
      }));
      setTimeout(() => {
        this.exerciseForm.controls.muscles.patchValue({
          primary: exercisePrimaryMuscles,
          secondary: exerciseSecondaryMuscles,
        });
      }, 0);
    }
  }

  changeIfSelectionPossible(selectedMuscles: IMuscle[], selectOrigin: "primary" | "secondary") {
    if (selectOrigin === "primary") {
      this.changeActiveSelectCheckboxState(this.secondaryMuscles, selectedMuscles);
    } else {
      this.changeActiveSelectCheckboxState(this.primaryMuscles, selectedMuscles);
    }
  }

  changeActiveSelectCheckboxState(toChange: IMuscleDisplay[], selectOptions: IMuscle[]) {
    selectOptions.forEach((muscle: IMuscle) => {
      const muscleToChangeIndex = toChange.findIndex((m) => m._id === muscle._id);
      if (muscleToChangeIndex !== -1) {
        toChange[muscleToChangeIndex].inactive = true;
      }
    });
    // if deleted
    const toChangeIds = toChange.map((muscle) => muscle._id);
    const selectedMusclesIds = selectOptions.map((muscle) => muscle._id);
    toChangeIds.map((id, index) => {
      if (!selectedMusclesIds.includes(id)) {
        toChange[index].inactive = false;
      }
    });
  }

  onSubmit() {
    if (this.exerciseForm.valid) {
    } else {
      return this.validationService.validateAllFormFields(this.exerciseForm);
    }

    const formalizedForm = this.exerciseService.getFormalizedForm(this.exerciseForm.value);
    if (this.openedModalData.edit) {
      const exerciseId = this.exerciseToEdit._id;
      this.exerciseService
        .update(exerciseId!, formalizedForm)
        .pipe(takeUntil(this.destroyer$))
        .subscribe((res) => {
          this.popupService.showActionMessage(res, "Error in updating exercise", "Exericse Edited Successfully!");
        });
    } else {
      this.exerciseService
        .create(formalizedForm)
        .pipe(takeUntil(this.destroyer$))
        .subscribe((res) => {
          this.popupService.showActionMessage(res, "Error in creating exercise", "Exericse Created Successfully!");
        });
    }
  }

  ngOnDestroy(): void {
    this.destroyer$.next();
    this.destroyer$.complete();
  }
}

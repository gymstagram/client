import { Component, OnInit } from "@angular/core";
import { IMuscle, IMuscleGroup } from "gymstagram-common";
import { PrimeNGConfig } from "primeng/api";
import { Observable, of } from "rxjs";
import { difficulties, Resourse } from "src/app/consts";
import { EntityModalActionsService } from "src/app/modules/core/services/entity-modal-actions.service";
import { EntitySearchService } from "src/app/modules/core/services/entity-search.service";
import { MuscleGroupService } from "src/app/modules/core/services/muscle-group.service";
import { MuscleService } from "src/app/modules/core/services/muscle.service";
import { PageResult } from "src/app/modules/shared/entity-display/entity-display.component";
import { FilteredSelect } from "src/app/modules/shared/filtered-search/filtered-search.component";
import { ExerciseService } from "../../services/exercise.service";
import { ExerciseFormComponent } from "../exercise-form/exercise-form.component";
import { ExerciseInfoDisplayComponent } from "../exercise-info-display/exercise-info-display.component";

@Component({
  selector: "app-exercise-container",
  templateUrl: "./exercise-container.component.html",
  styleUrls: ["./exercise-container.component.scss"],
  providers: [
    EntityModalActionsService,
    {
      provide: "modalData",
      useValue: {
        resourse: Resourse.exercise,
        infoModal: ExerciseInfoDisplayComponent,
        mutationForm: ExerciseFormComponent,
      },
    },
  ],
})
export class ExerciseContainerComponent implements OnInit {
  exercisesPage$: Observable<PageResult>;
  Resourse = Resourse;
  filteredSearches: FilteredSelect[] = [];
  muscleGroups$: Observable<IMuscleGroup[]>;
  muscles$: Observable<IMuscle[]>;
  difficulties = difficulties;
  constructor(
    private primengConfig: PrimeNGConfig,
    private exerciseService: ExerciseService,
    private entitySearchService: EntitySearchService,
    private entityModalActionService: EntityModalActionsService,
    private musclesService: MuscleService,
    private muscleGroupsService: MuscleGroupService
  ) {}

  ngOnInit(): void {
    this.muscles$ = this.musclesService.fetchAll();
    this.muscleGroups$ = this.muscleGroupsService.fetchAll();
    this.exercisesPage$ = this.exerciseService.getLatestPage();
    this.setFilteredSearches();
    this.fetchNextPage({ pageNumber: 1 });
    this.primengConfig.ripple = true;
  }
  setFilteredSearches() {
    // initialize the filtered search
    const muscleGroupSearch: FilteredSelect = {
      options$: this.muscleGroups$,
      placeholder: "Select Muscle Group",
      selectionLimit: 1,
      name: "muscleGroup",
      showHeader: true,
      searchUrlBy: "_id",
    };

    const musclesSearch: FilteredSelect = {
      options$: this.muscles$,
      placeholder: "Select Muscles",
      name: "muscles",
      showHeader: true,
      searchUrlBy: "_id",
    };

    const difficultySearch: FilteredSelect = {
      searchUrlBy: "level",
      options$: of(difficulties),
      placeholder: "Difficulty Level",
      selectionLimit: 1,
      name: "difficultyLevel",
      showHeader: false,
    };
    this.filteredSearches.push(muscleGroupSearch, musclesSearch, difficultySearch);
  }

  deleteExercise(exercise: any) {
    this.entityModalActionService.deleteWithConsent(exercise);
  }

  displayExerciseInfo(exercise: any) {
    this.entityModalActionService.displayInfo(exercise);
  }

  editExercise(exercise: any) {
    this.entityModalActionService.editEntity(exercise);
  }

  addExercise() {
    this.entityModalActionService.addEntity();
  }

  fetchNextPage(page: any) {
    const currentFilter = this.exerciseService.getFilter();
    this.exerciseService.setSearchFilter({ ...currentFilter, pageNumber: page.pageNumber }).fetchWithFilter();
  }
}

import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { ExerciseContainerComponent } from "./components/exercise-container/exercise-container.component";
import { ExerciseFormComponent } from "./components/exercise-form/exercise-form.component";
import { ExerciseInfoDisplayComponent } from "./components/exercise-info-display/exercise-info-display.component";
import { ExerciseRoutingModule } from "./exercise-routing.module";

const components: any = [ExerciseInfoDisplayComponent, ExerciseFormComponent, ExerciseContainerComponent];

@NgModule({
  declarations: [...components],
  imports: [ExerciseRoutingModule, SharedModule],
  providers: [],
})
export class ExerciseModule {}

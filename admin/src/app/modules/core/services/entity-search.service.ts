import { Injectable } from "@angular/core";
import { ServiceLocator } from "src/app/app.component";
import { Resourse } from "src/app/consts";
import { Filters } from "src/app/consts/filters";
import { EntityServiceFactory } from "./service-factory.service";

@Injectable({ providedIn: "root" })
export class EntitySearchService {
  constructor() {}

  private lastFilterSearched: Map<Resourse, any> = new Map();

  searchEntity(filter, resourse: Resourse) {
    const currentService = ServiceLocator.get(EntityServiceFactory).getServiceInstance(resourse);
    currentService.setSearchFilter(filter).fetchWithFilter();
  }

  resetSearchPage(filtersToReset: Filters[], resourse: Resourse) {
    const currentService = ServiceLocator.get(EntityServiceFactory).getServiceInstance(resourse);
    const currentFilter = currentService.getFilter();
    for (const filter of filtersToReset) {
      delete currentFilter[filter];
    }
    currentService.setSearchFilter({ ...currentFilter, pageNumber: 1 }).fetchWithFilter();
  }

  getlastFilterSearched(resourse: Resourse) {
    return this.lastFilterSearched.get(resourse);
  }

  setLastFilteredSearched(resourse: Resourse, filter: any) {
    this.lastFilterSearched.set(resourse, filter);
  }
}

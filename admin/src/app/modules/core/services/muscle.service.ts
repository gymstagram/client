import { Injectable } from "@angular/core";
import { IMuscle } from "gymstagram-common";
import { ResourseApi } from "src/app/consts";
import { CrudService } from "./generic.crud.service";

@Injectable({
  providedIn: "root",
})
export class MuscleService extends CrudService<IMuscle> {
  constructor() {
    super(ResourseApi.muscle);
  }
}

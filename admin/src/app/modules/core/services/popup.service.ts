import { Injectable } from "@angular/core";
import { MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";

interface ModalData {
  header?: string;
  data?: any;
}

@Injectable({
  providedIn: "root",
})
export class PopupService {
  constructor(private dialogService: DialogService, private messageService: MessageService) {}
  private openedDialog: DynamicDialogRef;
  showModal(component: any, toRender: ModalData) {
    const { data, header } = toRender;
    this.openedDialog = this.dialogService.open(
      component,
      {
        header,
        data,
      } ?? {}
    );
  }

  showActionMessage(actionResponse: any, failMessage?: string, successMessage?: string) {
    this.messageService.add({
      severity: actionResponse ? "success" : "error",
      summary: actionResponse ? "Success" : "Error",
      detail: actionResponse ? successMessage : failMessage,
    });

    this.openedDialog.close();
  }

  displayError(error: string) {
    this.messageService.add({
      severity: "error",
      summary: "Error",
      detail: error,
    });
  }

  closeOpenedDialog() {
    this.openedDialog?.close();
  }
}

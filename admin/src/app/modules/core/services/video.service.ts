import { Injectable } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";

enum VideoPrefix {
  YOUTUBE1 = "https://www.youtube.com/watch?v=",
  YOUTUBE2 = "https://youtu.be/",
}
@Injectable({
  providedIn: "root",
})
export class VideoService {
  constructor(private sanitizer: DomSanitizer) {}

  getVideoSafeUrl(videoUrl: string): SafeResourceUrl {
    const prefix = videoUrl.startsWith(VideoPrefix.YOUTUBE1) ? VideoPrefix.YOUTUBE1 : VideoPrefix.YOUTUBE2;
    if (!prefix) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(videoUrl);
    }
    //https://www.youtube.com/watch?v=1QxtSW2vO1E
    const youtubeVideoId = videoUrl.startsWith(VideoPrefix.YOUTUBE1)
      ? videoUrl.split(VideoPrefix.YOUTUBE1)[1]
      : videoUrl.split(VideoPrefix.YOUTUBE2)[1];
    const res = this.sanitizer.bypassSecurityTrustResourceUrl("http://www.youtube.com/embed/" + youtubeVideoId);
    return res;
  }

  formalizeYoutubeUrl(videoUrl: string) {
    return videoUrl?.split("&")?.[0];
  }
}

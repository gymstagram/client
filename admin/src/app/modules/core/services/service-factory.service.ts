import { Injectable } from "@angular/core";
import { ServiceLocator } from "src/app/app.component";
import { Resourse } from "src/app/consts/resourses";
import { ExerciseService } from "../../exercise/services/exercise.service";
import { TrainingService } from "../../training/services/training.service";
import { MuscleGroupService } from "./muscle-group.service";
import { MuscleService } from "./muscle.service";

@Injectable({
  providedIn: "root",
})
export class EntityServiceFactory {
  constructor() {}

  getServiceInstance<T>(resourseName: Resourse) {
    switch (resourseName) {
      case Resourse.exercise: {
        return ServiceLocator.get(ExerciseService);
      }
      case Resourse.muscle: {
        return ServiceLocator.get(MuscleService);
      }
      case Resourse.muscleGroup: {
        return ServiceLocator.get(MuscleGroupService);
      }
      case Resourse.training: {
        return ServiceLocator.get(TrainingService);
      }
      default:
        throw new Error("No Service of that resourse");
    }
  }
}

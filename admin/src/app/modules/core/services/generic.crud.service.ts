import { HttpParams } from "@angular/common/http";
import { Observable, of, Subject } from "rxjs";
import { filter, share, shareReplay, switchMap, tap } from "rxjs/operators";
import { ServiceLocator } from "src/app/app.component";
import { ResourseApi } from "src/app/consts";
import { environment } from "src/environments/environment";
import { PageResult } from "../../shared/entity-display/entity-display.component";
import { HttpService } from "./http.service";

export abstract class CrudService<T extends { _id?: string }> {
  constructor(resourseEnpoint: ResourseApi) {
    this.resourseEndpoint = `${environment.endpoint}/${resourseEnpoint}`;
  }
  private searchFilter: any;
  private totalFilteredEntities: number;
  resourseEndpoint;
  pageFetched$: Subject<PageResult> = new Subject();
  totalFilteredEntities$: Subject<number> = new Subject();
  cachedEntities$: Observable<T[]>;
  protected http = ServiceLocator.get(HttpService);

  // for loading the resourese that are populated in dropdown and never change(muscles for example)
  fetchAll() {
    if (this.cachedEntities$) {
      return this.cachedEntities$;
    }
    this.cachedEntities$ = this.http.get<{ entities: T[]; totalFilteredRecords: number }>(this.resourseEndpoint).pipe(
      filter((res) => !!res),
      switchMap((res) => of(res.entities)),
      shareReplay(1)
    );
    return this.cachedEntities$;
  }

  fetchOne<T>(id: string) {
    return this.http.get<T>(`${this.resourseEndpoint}/${id}`).pipe(
      share(),
      filter((res) => !!res)
    );
  }

  fetch(subEndpoint: string) {
    return this.http.get(subEndpoint);
  }

  fetchWithFilter() {
    return this.http
      .get<{ entities: T[]; totalFilteredRecords: number }>(
        `${this.resourseEndpoint}`,
        new HttpParams({
          fromObject: this.searchFilter,
        })
      )
      .pipe(
        share(),
        filter((res) => !!res),
        switchMap((res) => {
          this.pageFetched$.next(res);
          return of(res.entities);
        })
      )
      .subscribe();
  }

  create(entity: T) {
    return this.http.post(this.resourseEndpoint, entity).pipe(
      share(),

      tap(() => {
        return this.fetchWithFilter();
      })
    );
  }
  delete(id: string) {
    return this.http.delete(this.resourseEndpoint, id).pipe(
      share(),

      tap(() => {
        return this.fetchWithFilter();
      })
    );
  }

  update(id: string, newEntity: T) {
    return this.http.put(this.resourseEndpoint, id, newEntity).pipe(
      share(),
      tap((res) => {
        return this.fetchWithFilter();
      })
    );
  }

  setSearchFilter(filter: any) {
    this.searchFilter = filter;
    return this;
  }

  getFilter() {
    return this.searchFilter;
  }

  getLatestPage() {
    return this.pageFetched$.asObservable();
  }

  getTotalFilteredEntities() {
    return this.totalFilteredEntities$.asObservable();
  }
}

import { Inject, Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { ServiceLocator } from "src/app/app.component";
import { Resourse } from "src/app/consts";
import { AuthService } from "../../auth/services/auth.service";
import { PopupService } from "./popup.service";
import { EntityServiceFactory } from "./service-factory.service";

export class ModalData {
  resourse: Resourse;
  infoModal: any;
  mutationForm: any;
}

@Injectable()
export class EntityModalActionsService {
  constructor(
    @Inject("modalData") private modalData: ModalData,
    public dialogService: DialogService,
    private messageService: MessageService,
    private popupService: PopupService,
    private authService: AuthService,
    private router: Router
  ) {}
  private currentService = ServiceLocator.get(EntityServiceFactory).getServiceInstance(this.modalData.resourse);
  deleteWithConsent(entity: any) {
    if (entity._id) {
      this.currentService.delete(entity._id).subscribe((res) => {
        this.popupService.showActionMessage(
          res,
          `Error in deleting ${this.modalData.resourse}`,
          `${this.modalData.resourse} Deleted Successfully`
        );
      });
    }
  }

  displayInfo(entity: any) {
    return this.popupService.showModal(this.modalData.infoModal, { header: `${entity.name}`, data: entity });
  }

  editEntity(entity: any) {
    return this.popupService.showModal(this.modalData.mutationForm, {
      header: `Edit ${this.modalData.resourse}`,
      data: { edit: true, entityToEdit: entity },
    });
  }

  addEntity() {
    return this.popupService.showModal(this.modalData.mutationForm, {
      header: `Add ${this.modalData.resourse}`,
      data: { edit: false },
    });
  }
}

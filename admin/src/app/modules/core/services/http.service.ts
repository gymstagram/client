import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class HttpService {
  constructor(private http: HttpClient) {}

  get<T>(url: string, params?: HttpParams, headers?: HttpHeaders) {
    return this.http.get<T>(url, { headers, params });
  }
  post(url: string, body: any, params?: HttpParams, headers?: HttpHeaders) {
    return this.http.post<any>(url, body, { headers, params });
  }

  delete(url: string, id: string, headers?: HttpHeaders) {
    return this.http.delete<{ deleted: string }>(`${url}/${id}`);
  }

  put(url: string, id: string, body: any, headers?: HttpHeaders) {
    return this.http.put<{ updated: string }>(`${url}/${id}`, body, {
      headers,
    });
  }
}

import { Injectable } from "@angular/core";
import { IPost } from "gymstagram-common";
import { ResourseApi } from "src/app/consts";
import { CrudService } from "./generic.crud.service";

@Injectable({
  providedIn: "root",
})
export class PostsService extends CrudService<IPost> {
  constructor() {
    super(ResourseApi.post);
  }

  getMostLikedPosts() {
    const mostLikedURL = `${this.resourseEndpoint}/mostLiked`;
    return this.fetch(mostLikedURL);
  }
}

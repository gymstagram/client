import { Injectable } from "@angular/core";
import { IMuscleGroup } from "gymstagram-common";
import { ResourseApi } from "src/app/consts";
import { CrudService } from "./generic.crud.service";
import { MuscleService } from "./muscle.service";

@Injectable({
  providedIn: "root",
})
export class MuscleGroupService extends CrudService<IMuscleGroup> {
  constructor(private muscleService: MuscleService) {
    super(ResourseApi.muscleGroup);
  }
}

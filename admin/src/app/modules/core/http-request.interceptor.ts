import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { Observable, throwError } from "rxjs";
import { catchError, finalize, timeout } from "rxjs/operators";
import { AuthService } from "../auth/services/auth.service";
import { LoaderService } from "./services/loader.service";
import { PopupService } from "./services/popup.service";

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  constructor(
    private messageService: MessageService,
    private loaderService: LoaderService,
    private router: Router,
    private popupService: PopupService,
    private authService: AuthService
  ) {}
  timer: any;
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => this.loaderService.setLoading(true), 150);

    return next.handle(request).pipe(
      timeout(5000),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401 || error.status === 403) {
          this.popupService.closeOpenedDialog();
          this.authService.logout();
        }
        console.error("Error from error interceptor", error);
        console.log(` %c== Request Failed ${request.url} == `, "color: #ff0000");
        console.log(request);

        this.popupService.displayError(error?.error?.error || error.message);
        return throwError(error);
      }),
      finalize(() => {
        this.loaderService.setLoading(false);
        if (this.timer) {
          clearTimeout(this.timer);
        }
      })
    ) as Observable<HttpEvent<any>>;
  }
}

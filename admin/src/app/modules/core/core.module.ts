import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MessageService } from "primeng/api";
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";

const popupServices = [MessageService, DialogService, DynamicDialogRef];

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [...popupServices],
})
export class CoreModule {}

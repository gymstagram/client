import { Injectable } from "@angular/core";
import { FormArray, FormControl, FormGroup } from "@angular/forms";

@Injectable({
  providedIn: "root",
})
export class ValidationService {
  constructor() {}

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      }
      if (control instanceof FormArray) {
        console.log("in validation service", control);

        control.markAllAsTouched();
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}

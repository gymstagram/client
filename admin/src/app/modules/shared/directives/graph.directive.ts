import { Directive, ElementRef, HostListener, Input, OnChanges, SimpleChanges } from "@angular/core";
import * as d3 from "d3";
import { get, max } from "lodash";

@Directive({
  selector: "[appGraph]",
})
export class GraphDirective implements OnChanges {
  @Input("appGraph") data: any[];
  @Input() width: number;
  @Input() height: number;
  @Input() margin: number = 50;
  yAxisName: string;
  xAxisName: string;
  graphClassName: string;

  constructor(element: ElementRef) {
    this.graphClassName = (element.nativeElement as HTMLElement).className;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data.currentValue) {
      this.data = changes.data.currentValue;
      [this.xAxisName, this.yAxisName] = Object.keys(this.data[0]);
      this.drawChart();
    }
  }

  @HostListener("window:resize")
  drawChart() {
    // Set the dimensions of the canvas / graph
    document.querySelector("svg")?.remove();
    // Set the scales ranges
    const xScale = d3
      .scaleBand()
      .range([0, this.width])
      .domain(this.data.map((d) => get(d, this.xAxisName)))
      .padding(1);

    const yScale = d3
      .scaleLinear()
      .domain([0, Math.ceil(max(this.data.map((p) => p && get(p, this.yAxisName))) || 0)])
      .range([this.height, 0]);

    // Define the axes
    const xAxis = (d3 as any).axisBottom().scale(xScale);
    const yAxis = (d3 as any).axisLeft().scale(yScale);
    // create a line
    const line = d3.line();

    // Add the svg canvas
    const svg = d3
      .select(`.${this.graphClassName}`)
      .append("svg")
      .attr("height", this.height + this.margin + this.margin);

    const artboard = svg.append("g").attr("transform", "translate(" + this.margin + "," + this.margin + ")");

    // draw the line created above
    const path = artboard
      .append("path")
      .data([this.data])
      .style("fill", "none")
      .style("stroke", "steelblue")
      .style("stroke-width", "2px");

    // Add the X Axis
    const xAxisEl = artboard.append("g").attr("transform", "translate(0," + this.height + ")");

    // Add the Y Axis
    // we aren't resizing height in this demo so the yAxis stays static, we don't need to call this every resize
    const yAxisEl = artboard.append("g").call(yAxis);
    // reset the width
    this.width = parseInt((d3 as any).select(`.${this.graphClassName}`).style("width"), 10) - this.margin - this.margin;

    // set the svg dimensions
    svg.attr("width", this.width + this.margin + this.margin);

    // Set new range for xScale
    xScale.range([0, this.width]);

    // give the x axis the resized scale
    xAxis.scale(xScale);

    // draw the new xAxis
    xAxisEl.call(xAxis);

    // specify new properties for the line
    (line as any).x((d) => xScale(get(d, this.xAxisName))).y((d) => yScale(get(d, this.yAxisName)));

    // draw the path based on the line created above
    path.attr("d", line);
  }
}

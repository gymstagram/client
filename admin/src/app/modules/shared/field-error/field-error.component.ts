import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from "@angular/core";

@Component({
  selector: "app-field-error-display",
  templateUrl: "./field-error.component.html",
  styleUrls: ["./field-error.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FieldErrorComponent implements OnInit {
  @Input() errorMsg: string = "Required";
  @Input() displayError: boolean;

  constructor() {}

  ngOnInit(): void {}
}

import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MenuItem } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { AuthService } from "../../auth/services/auth.service";
import { PopupService } from "../../core/services/popup.service";
import { NavVerticalMenuComponent } from "../nav-vertical-menu/nav-vertical-menu.component";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DialogService],
})
export class NavbarComponent implements OnInit {
  constructor(
    private popupService: PopupService,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  items: MenuItem[];
  userInitials: string;
  ngOnInit() {
    this.userInitials = this.authService.getAuthUserInitials();
  }

  openMenu() {
    this.popupService.showModal(NavVerticalMenuComponent, {});
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl("/login");
  }
}

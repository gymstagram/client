import { ChangeDetectionStrategy, Component, HostListener, OnInit } from "@angular/core";
import { AuthService } from "../../auth/services/auth.service";
import { PopupService } from "../../core/services/popup.service";

@Component({
  selector: "app-nav-vertical-menu",
  templateUrl: "./nav-vertical-menu.component.html",
  styleUrls: ["./nav-vertical-menu.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavVerticalMenuComponent implements OnInit {
  @HostListener("window:resize")
  onResize(e: any) {
    if (window.innerWidth > 900) {
      this.closeMenu();
    }
  }

  constructor(public popupService: PopupService, private authService: AuthService) {}

  ngOnInit(): void {}

  logout() {
    this.closeMenu();
    this.authService.logout();
  }

  closeMenu() {
    this.popupService.closeOpenedDialog();
  }
}

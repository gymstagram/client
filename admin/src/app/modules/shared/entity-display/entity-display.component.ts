import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import { SafeUrl } from "@angular/platform-browser";
import { IExercise, ITraining } from "gymstagram-common";
import { ConfirmationService, PrimeNGConfig } from "primeng/api";
import { Paginator } from "primeng/paginator";
import { Subject } from "rxjs";
import { Resourse } from "src/app/consts";
import { LoaderService } from "../../core/services/loader.service";
import { VideoService } from "../../core/services/video.service";

type DisplaybleEntity = (ITraining | IExercise) & { sanitizedVideoToDisplay?: SafeUrl };

export interface PageRequest {
  pageNumber: number;
  resourse: Resourse;
}

export interface PageResult {
  entities: any[];
  totalFilteredRecords: number;
}
@Component({
  selector: "app-entity-display",
  templateUrl: "./entity-display.component.html",
  styleUrls: ["./entity-display.component.scss"],
  providers: [ConfirmationService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntityDisplayComponent<T extends DisplaybleEntity> implements OnChanges, OnInit, OnDestroy {
  @Output() entityDeleted: EventEmitter<T> = new EventEmitter();
  @Output() entityEdited: EventEmitter<T> = new EventEmitter();
  @Output() entityDisplayed: EventEmitter<T> = new EventEmitter();
  @Output() pageChanged: EventEmitter<PageRequest> = new EventEmitter();
  @Input() resourse: Resourse;
  @Input() pageResult: PageResult | null;

  @ViewChild("paginator") paginator: Paginator;
  isLoadingVideo: boolean = true;
  destroyer$: Subject<void> = new Subject();
  constructor(
    private confirmationService: ConfirmationService,
    private primengConfig: PrimeNGConfig,
    private videoService: VideoService,
    private loadingService: LoaderService
  ) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.pageResult.currentValue?.entities?.length) {
      this.pageResult?.entities.map(
        (entity) => (entity.sanitizedVideoToDisplay = this.videoService.getVideoSafeUrl(entity.video))
      );
      this.isLoadingVideo = true;
    }
  }

  emitDelete(entity: T) {
    this.confirmationService.confirm({
      message: `Are you sure that you want to delete ${entity.name}?`,
      header: "Confirmation",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        this.confirmationService.close();
        this.entityDeleted.emit(entity);
      },
      reject: () => {
        this.confirmationService.close();
      },
    });
  }
  emitEdit(entity: T) {
    this.entityEdited.emit(entity);
  }
  emitDisplayInfo(entity: T) {
    this.entityDisplayed.emit(entity);
  }

  getNextPage({ page }: any) {
    this.pageChanged.emit({ resourse: this.resourse, pageNumber: page + 1 });
  }

  ngOnDestroy(): void {
    this.destroyer$.next();
    this.destroyer$.complete();
  }
}

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  QueryList,
  ViewChildren,
} from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { filter, find, get, identity, isArray, isEmpty, isString, omit, pick, pickBy } from "lodash";
import { MultiSelect } from "primeng/multiselect";
import { Observable } from "rxjs";
import { Resourse } from "src/app/consts";
import { Filters } from "src/app/consts/filters";
import { EntitySearchService } from "../../core/services/entity-search.service";
import { LoaderService } from "../../core/services/loader.service";

export interface FilteredSelect {
  options$: Observable<any[]>;
  selectionLimit?: number;
  placeholder: string;
  name: string;
  showHeader: boolean;
  searchUrlBy: string; // what field should be taken to the search url
}

@Component({
  selector: "app-filtered-search",
  templateUrl: "./filtered-search.component.html",
  styleUrls: ["./filtered-search.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilteredSearchComponent implements OnInit, AfterViewInit {
  entitySearched: any;
  @Input() filterSearches: FilteredSelect[] = [];
  @Input() resourse: Resourse;
  @ViewChildren(MultiSelect) ms: QueryList<MultiSelect>;
  // selectOptions = new Map<string, any[]>();
  searchForm: FormGroup;
  isLoading$: Observable<boolean>;
  constructor(
    private fb: FormBuilder,
    private loaderService: LoaderService,
    private entitySearchService: EntitySearchService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.searchForm = this.fb.group({});
    this.isLoading$ = this.loaderService.isLoading;
    const setFilter = this.entitySearchService.getlastFilterSearched(this.resourse);
    this.filterSearches.map((fs) => this.searchForm.addControl(fs.name, this.fb.control(get(setFilter, fs.name))));
    this.searchForm.addControl("name", this.fb.control(get(setFilter, "name")));

    // todo get the params from the url
    this.activatedRoute.queryParams.subscribe(async (filters) => {
      // todo get the select options

      const formValueFromParams = await this.selectFiltersFromParams(filters);
      this.entitySearchService.setLastFilteredSearched(this.resourse, formValueFromParams);
      const setFilter = this.entitySearchService.getlastFilterSearched(this.resourse);
      setTimeout(() => {
        for (const filter in setFilter) {
          this.searchForm.patchValue({ [filter]: get(setFilter, filter) });
        }
        const searchFilter = this.createSearchFilterParams();
        this.entitySearchService.searchEntity(searchFilter, this.resourse); // todo move to subscribe
      });
    });
  }

  ngAfterViewInit(): void {
    // another patch to prime ng bugs !!
    this.ms.forEach((multiselect) => {
      multiselect.hide = () => {
        multiselect.overlayVisible = false;
        multiselect.unbindDocumentClickListener();
        if (multiselect.resetFilterOnHide) {
          // tslint:disable-next-line: no-unused-expression
          multiselect.filterInputChild && (multiselect.filterInputChild.nativeElement.value = "");
          (multiselect._filterValue as any) = null;
          (multiselect._filteredOptions as any) = null;
        }
        multiselect.onPanelHide.emit();
        multiselect.cd.markForCheck();
      };
    });
  }

  reset() {
    this.filterSearches.map((fs) => this.searchForm.patchValue({ [fs.name]: null }));
    this.clearInputSearch();
  }

  search(event?: any) {
    event?.preventDefault();
    const searchFilter = this.createSearchFilterParams();

    this.router.navigate([], {
      queryParams: searchFilter,
      replaceUrl: true,
    });
  }

  clearInputSearch() {
    this.searchForm.patchValue({ name: null });
  }

  private createSearchFilterParams() {
    const finalSearchFilter = pick(this.searchForm.value, "name");
    Object.keys(this.searchForm.value).map((filter) => {
      const filterSearchObject = find(this.filterSearches, (fs) => fs.name === filter);
      finalSearchFilter[filter] =
        (filterSearchObject?.searchUrlBy &&
          this.searchForm.value[filter]?.map((x) => get(x, filterSearchObject?.searchUrlBy))) ||
        this.searchForm.value[filter];
    });

    return pickBy(finalSearchFilter, (x) => !isEmpty(x)); // removes all null values
  }

  private getFullObjByValue = (arr: any[] | undefined, value: any) => {
    if (!arr) return null;
    for (const obj of arr) {
      if (Object.keys(obj).some((key) => obj[key] === value)) {
        return obj;
      }
    }
    return null;
  };

  private selectFiltersFromParams = async (filters: Params) => {
    const finalSelectedFilters = pick(filters, "name");
    for (const key in omit(filters, "name")) {
      let idsToSearch: any[] = [];
      if (!isNaN(+filters[key])) {
        idsToSearch = [+filters[key]];
      } else if (isString(filters[key])) {
        idsToSearch = [filters[key]];
      } else {
        idsToSearch = [...filters[key]];
      }
      const filterSearchObject = find(this.filterSearches, (fs) => fs.name === key);
      const options = await filterSearchObject?.options$.toPromise();
      const fullSelectObjectes = idsToSearch.map((id) => this.getFullObjByValue(options, id));
      finalSelectedFilters[key] = fullSelectObjectes;
    }
    return finalSelectedFilters;
  };
}

import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PrimeNgModule } from "./../primeng/primeng.module";
import { GraphDirective } from "./directives/graph.directive";
import { EntityDisplayComponent } from "./entity-display/entity-display.component";
import { FieldErrorComponent } from "./field-error/field-error.component";
import { FilteredSearchComponent } from "./filtered-search/filtered-search.component";
import { NavVerticalMenuComponent } from "./nav-vertical-menu/nav-vertical-menu.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { DifficultyDisplayPipe } from "./pipes/diificulty.pipe";

const sharedComponents = [
  FieldErrorComponent,

  NavbarComponent,
  EntityDisplayComponent,
  NavVerticalMenuComponent,
  FilteredSearchComponent,
];
const sharedPipes = [DifficultyDisplayPipe];
const sharedDirectives = [GraphDirective];
const sharedModules = [CommonModule, FormsModule, ReactiveFormsModule, HttpClientModule, PrimeNgModule];
@NgModule({
  declarations: [...sharedComponents, ...sharedPipes, GraphDirective],
  imports: [...sharedModules],
  exports: [...sharedComponents, ...sharedModules, ...sharedPipes, ...sharedDirectives],
})
export class SharedModule {}

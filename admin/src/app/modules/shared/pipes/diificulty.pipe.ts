import { Pipe, PipeTransform } from "@angular/core";
import { Difficulty } from "gymstagram-common";

@Pipe({
  name: "difficulty",
})
export class DifficultyDisplayPipe implements PipeTransform {
  transform(value: number): string {
    return Difficulty[value];
  }
}

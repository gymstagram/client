import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { PopupService } from "../core/services/popup.service";
import { AuthService } from "./services/auth.service";

@Injectable({ providedIn: "root" })
export class LoginActivate implements CanActivate {
  constructor(private authService: AuthService, private router: Router, private popupService: PopupService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.authService.isAuthenticated()) {
      if (this.authService.getToken()) {
        this.popupService.displayError("Session Timeout");
      }
      this.authService.logout();
      return false;
    }
    return true;
  }
}

import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { LoaderService } from "../../core/services/loader.service";
import { AuthService } from "../services/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(private loaderService: LoaderService, private authService: AuthService, private router: Router) {}
  username: string;
  password: string;
  isLoading$: Observable<boolean>;
  destroyer$: Subject<boolean> = new Subject();
  ngOnInit(): void {
    this.isLoading$ = this.loaderService.isLoading;
  }

  submitForm(form: NgForm) {
    const { username, password } = form.value;
    this.authService
      .login({ username, password })
      .pipe(takeUntil(this.destroyer$))
      .subscribe((res) => this.router.navigateByUrl("/exercises"));
  }

  ngOnDestroy(): void {
    this.destroyer$.next();
    this.destroyer$.complete();
  }
}

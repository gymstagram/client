import { CommonModule } from "@angular/common";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { HttpRequestInterceptor } from "../core/http-request.interceptor";
import { SharedModule } from "../shared/shared.module";
import { LoginComponent } from "./login/login.component";
import { TokenInterceptor } from "./token.interceptor";

@NgModule({
  declarations: [LoginComponent],
  imports: [CommonModule, SharedModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpRequestInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
})
export class AuthModule {}

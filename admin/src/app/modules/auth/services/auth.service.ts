import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { IUser } from "gymstagram-common";
import jwtDecode from "jwt-decode";
import { BehaviorSubject, Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { HttpService } from "../../core/services/http.service";

export type User = Pick<IUser, "password" | "username">;

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(private http: HttpService, private router: Router) {}
  loginEndpoint = `${environment.endpoint}/users/login`;
  isAuthenticated$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.isAuthenticated());
  login(user: User): Observable<{ username: string; accessToken: string }> {
    const loginBody = { ...user, app: "admin" };
    return this.http.post(this.loginEndpoint, loginBody).pipe(
      tap((res) => {
        this.setToken(res.accessToken);
        this.isAuthenticated$.next(true);
      })
    );
  }

  setToken(token: string) {
    localStorage.setItem("t", token);
  }

  getToken() {
    return localStorage.getItem("t");
  }

  isTokenExpired(): boolean {
    const token = this.getToken();

    if (!token) {
      return true;
    }
    return jwtDecode<any>(token).exp <= Date.now() / 1000;
  }

  getTokenIdentity(): string | null {
    const token = this.getToken();
    return token ? jwtDecode<any>(token).identity : null;
  }

  getAuthenticatedUser() {
    return this.isAuthenticated$.asObservable();
  }

  isAuthenticated() {
    return !this.isTokenExpired();
  }
  getAuthUserInitials() {
    const identity = this.getTokenIdentity();
    return identity?.split(" ").length === 1
      ? identity
      : this.getTokenIdentity()
          ?.split(" ")
          .map((n) => n[0].toUpperCase())
          .join("") || "";
  }
  logout() {
    this.isAuthenticated$.next(false);
    localStorage.clear();
    this.router.navigateByUrl("/login");
  }
}

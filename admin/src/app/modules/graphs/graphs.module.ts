import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { TrainingModule } from "../training/training.module";
import { GraphsRoutingModule } from "./graphs-routing.module";
import { GraphsComponent } from "./graphs.component";

@NgModule({
  declarations: [GraphsComponent],
  imports: [CommonModule, GraphsRoutingModule, SharedModule, TrainingModule],
})
export class GraphsModule {}
